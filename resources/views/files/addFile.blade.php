@extends('layouts.main')

@section('title' , 'Add File')

@section('styles')
    <style>
      .add-file-form{
        margin:0px 20%;
      }
      .error{
        color:red;
        font-size:1rem;
      }
      .submit-btn{
        margin-top:20px; 
        margin-bottom:20px; 
      }
      #grouped_items{
        display:none;
      }
      @media screen and (max-width: 600px) {
        .add-file-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">
<form  class="add-file-form" enctype="multipart/form-data"
 action="{{url('addFile')}}" id="add_product" method="post">
@csrf

  <div class="form-group">
    <label for="productInput">File Name</label>
    <input type="text" name="name" class="form-control" id="fileInput" value="{{old('name')}}" aria-describedby="fileNameHelp" placeholder="Enter File name">
    @if($errors->has('name'))
    <small id="productNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productShortInput">File Category</label>
    <select name="category" class="form-control" id="">
      <option value="Leasing Contracts">Leasing Contracts</option>
      <option value="License Permits">License Permits</option>
      <option value="HR">HR</option>
      <option value="Permits">Permits</option>
      <option value="Operations">Operations</option>
      <option value="Establishment">Establishment</option>
    </select>
    @if($errors->has('category'))
    <small class="form-text error">{{ $errors->first('category') }}</small>
    @endif
  </div>

  <div class="form-group">
        <input type="file" name="file_url[]">
        @if($errors->has('file_url'))
          <small id="productImageHelp" class="form-text error">{{ $errors->first('file_url') }}</small>
        @endif
  </div>    

  <button id="submit" class="btn btn-primary submit-btn">Submit</button>
</form>

</div>
@endsection

@section('scripts')
@endsection