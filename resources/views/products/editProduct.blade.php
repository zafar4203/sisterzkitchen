@extends('layouts.main')

@section('title' , 'Edit Product')

@section('styles')
    <style>
      .edit-product-form{
        margin:0px 20%;
      }
    .error{
      color:red;
      font-size:1rem;
    }
    .submit-btn{
        margin-top:20px; 
        margin-bottom:20px; 
      }
      #grouped_items{
        display:none;
      }

      @media screen and (max-width: 600px) {
        .edit-product-form{
          margin:0px 5%;
        }
      }

    .custom-control-input:checked~.custom-control-label::before {
        color: #fff;
        border-color: #e74a3b;
        background-color: #e74a3b;
    }
    </style>
@endsection

@section('content')
<div class="container-fluid">

<form  class="edit-product-form" enctype="multipart/form-data"
 action="{{url('updateProduct')}}" id="edit_product" method="post">

 @if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif


@csrf
  <input type="hidden" name="id" value="{{$product->id}}">

  <div class="form-group">
    <label for="productInput">Product Name</label>
    <input type="text" name="name" class="form-control" value="{{$product->name}}" id="productInput" aria-describedby="productNameHelp" placeholder="Enter product name">
    @if($errors->has('name'))
    <small id="productNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productShortInput">Product Short Name</label>
    <input type="text" name="short_name" class="form-control" value="{{$product->short_name}}" id="productShortInput" aria-describedby="productShortNameHelp" placeholder="Enter product short name">
    @if($errors->has('short_name'))
    <small id="productShortNameHelp" class="form-text error">{{ $errors->first('short_name') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productPrice">Product Price</label>
    <input type="text" name="price" class="form-control" value="{{$product->price}}" id="productPrice" aria-describedby="productPriceHelp" placeholder="Enter product price">
    @if($errors->has('price'))
    <small id="productPriceHelp" class="form-text error">{{ $errors->first('price') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productPrice">Product Unit</label>
    <select name="unit" class="form-control" id="">
      <option @if($product->unit == "Kg") selected @endif value="Kg">Kg</option>
      <option @if($product->unit == "Dozen") selected @endif value="Dozen">Dozen</option>
    </select>
    @if($errors->has('unit'))
    <small id="productUnitHelp" class="form-text error">{{ $errors->first('unit') }}</small>
    @endif
  </div>

  <div class="form-group">
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="approve" value="0" name="block" @if($product->block == 0) checked @endif>
        <label class="custom-control-label" for="approve">Approve</label>
    </div>

    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="block" value="1" name="block" @if($product->block == 1) checked @endif>
        <label class="custom-control-label" for="block">Block</label>
      </div>
  </div>

  <label for="">Visible on</label>
  <div class="form-group">
    <div class="custom-control custom-checkbox custom-control-inline">
        <input type="checkbox" class="custom-control-input" id="web" name="web" @if($product->web == 1) checked @endif>
        <label class="custom-control-label" for="web">Web</label>
    </div>

    <div class="custom-control custom-checkbox custom-control-inline">
        <input type="checkbox" class="custom-control-input" id="mob" name="mob" @if($product->mob == 1) checked @endif>
        <label class="custom-control-label" for="mob">Mobile</label>
      </div>

    <div class="custom-control custom-checkbox custom-control-inline">
      <input type="checkbox" class="custom-control-input" id="adm" name="adm" @if($product->adm == 1) checked @endif>
      <label class="custom-control-label" for="adm">Admin</label>
    </div>
  </div>

  <div class="form-group">
        <input type="file" name="image[]" multiple>
        @if($errors->has('image'))
          <small id="productImageHelp" class="form-text error">{{ $errors->first('image') }}</small>
        @endif
  </div>    


  <button id="submit" class="btn submit-btn btn-danger">Submit</button>
</form>

</div>
@endsection
@section('scripts')
<script>
     $("#alert").fadeTo(2000, 500).slideUp(500, function(){
       $("#alert").slideUp(600);
       });

    </script>
@endsection