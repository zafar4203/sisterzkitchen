@extends('layouts.main')

@section('title' , 'Sales')

@section('styles')
    <link href="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <style>
      .error{
        color:red;
        font-size:1rem;
      }
      .ml-00{
          margin-left:1px !important;
      }
     .active-yr, .btn:hover {
        background-color: #4841a8;
        color: white;
      }
      .active-mn, .btn:hover {
        background-color: #4841a8;
        color: white;
      }
        .text-primary{
            color:#e74a3b !important;            
        }
.dates-row{
    z-index:9999;
    position:relative;
}
      @media screen and (max-width: 600px) {
        .btn-group{
          display:inline-block;
        }
        .mn{
          margin-top:10px;
        }
        .exp-btn {
            text-align:left !important;
            margin-top:20px;
        }
        .add-exp-btn{
            text-align:left !important;
            margin-top:20px;
        }        
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">

@if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800">Revenues</h1>

        <div class="row">
            <div class="col-md-6 col-12">
                <div class="btn-group" id="years">
                    <button type="button" id="2020" class="yr btn btn-danger">2020</button>
                    <button type="button" id="2021" class="ml-00 yr btn btn-danger">2021</button>
                    <button type="button" id="2022" class="ml-00 yr btn btn-danger">2022</button>
                    <button type="button" id="2023" class="ml-00 yr btn btn-danger">2023</button>
                </div>
            </div>

            <div class="col-md-6 add-exp-btn text-right">
                <a href="{{ url('addRevenue') }}"><button class="btn btn-danger">Add Revenue</button></a>
            </div>

            <div class="col-md-12 mt-2">
                <div class="btn-group" id="months">
                    <button type="button" id="1" class="btn mn btn-danger">Jan</button>
                    <button type="button" id="2" class="ml-00 mn btn btn-danger">Feb</button>
                    <button type="button" id="3" class="ml-00 mn btn btn-danger">March</button>
                    <button type="button" id="4" class="ml-00 mn btn btn-danger">April</button>
                    <button type="button" id="5" class="ml-00 mn btn btn-danger">May</button>
                    <button type="button" id="6" class="ml-00 mn btn btn-danger">June</button>
                    <button type="button" id="7" class="ml-00 mn btn btn-danger">July</button>
                    <button type="button" id="8" class="ml-00 mn btn btn-danger">Aug</button>
                    <button type="button" id="9" class="ml-00 mn btn btn-danger">Sep</button>
                    <button type="button" id="10" class="ml-00 mn btn btn-danger">Oct</button>
                    <button type="button" id="11" class="ml-00 mn btn btn-danger">Nov</button>
                    <button type="button" id="12" class="ml-00 mn btn btn-danger">Dec</button>
                </div>
            </div>            

        </div>


          <!-- DataTales Example -->
          <div class="card shadow mt-2 mb-4">
            <div class="card-body">
              
              <div class="table-responsive">
                <table class="table table-bordered" id="revenueTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Name</th>
                      <th>Description</th>
                      <th>Amount</th>
                      <th>Date</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->


          <!-- Logout Modal-->
  <div class="modal fade" id="deleteCategoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Are you sure to delete?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select <span class="error">"Delete"</span> below if you are ready to delete the Category.</div>
        <div class="modal-footer">
          <button class="btn btn-success" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" id="deleteModel" href="#">Delete</a>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')
  <!-- Page level plugins -->
  <script src="{{asset('public/vendor/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

  <!-- Page level custom scripts -->
  <script src="{{asset('public/js/demo/datatables-demo.js')}}"></script>

  <script>
     $("#alert").fadeTo(2000, 500).slideUp(500, function(){
       $("#alert").slideUp(600);
       });

        // Selection Area
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth() + 1;

        $('#'+year).addClass('active-yr');
        $('#'+month).addClass('active-mn');

        // Add active class to the current button (highlight it)
        var header = document.getElementById("years");
        var btns = header.getElementsByClassName("yr");
        for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function() {
        var current = document.getElementsByClassName("active-yr");
        current[0].className = current[0].className.replace(" active-yr", "");
        this.className += " active-yr";
        });
        }


                // Add active class to the current button (highlight it)
        var header = document.getElementById("months");
        var btns = header.getElementsByClassName("mn");
        for (var i = 0; i < btns.length; i++) {
          btns[i].addEventListener("click", function() {
          var current = document.getElementsByClassName("active-mn");
          current[0].className = current[0].className.replace(" active-mn", "");
          this.className += " active-mn";
          });
        }


        $('.yr').click(function(){
            year = this.id;
            getData();
        });

        $('.mn').click(function(){
            month = this.id;
            getData();
        });

       function delete_click(clicked_id){
          $('#deleteModel').attr("href","{{url('deleteRevenue')}}/"+clicked_id)
          $('#deleteCategoryModal').modal('show');
        }

        $('#revenueTable').DataTable({
              "processing":true,
              "serverside":true,
              "bPaginate": false,
              "bInfo" : false,
              "pageLength": 50,
              "ajax":"{{url('ajaxRevenues')}}/"+year+"/"+month,
              "columns":[
                {"data" : "id"},
                {"data" : "name"},
                {"data" : "description"},
                {"data" : "amount"},
                {"data" : "date"},
                {"data" : "action"},
              ],
              order:[0,'desc']
            });

        function getData(){
          $('#revenueTable').DataTable().ajax.url("{{url('ajaxRevenues')}}/"+year+"/"+month).load();
        }

  </script>
@endsection