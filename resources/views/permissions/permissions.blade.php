@extends('layouts.main')

@section('title' , 'Permissions')

@section('styles')
    <style>
      .custom-control-label{
        font-size:22px;
        color:#000000;
      }

    </style>
@endsection

@section('content')
<div class="container-fluid">

    @if(session()->has('message'))
        <div id="alert" class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif

      <h3>Permission</h3>

      <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6 mt-3">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" @if($sales) checked @endif id="sales">
                    <label class="custom-control-label" for="sales">Sales</label>
                  </div>
                </div>

                <div class="col-md-6 mt-3">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" @if($orders) checked @endif id="orders">
                    <label class="custom-control-label" for="orders">Orders</label>
                  </div>
                </div>

                <div class="col-md-6 mt-3">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" @if($products) checked @endif id="products">
                    <label class="custom-control-label" for="products">Products</label>
                  </div>
                </div>

                <div class="col-md-6 mt-3">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" @if($staff) checked @endif id="staff">
                    <label class="custom-control-label" for="staff">Staff</label>
                  </div>
                </div>

                <div class="col-md-6 mt-3">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" @if($financials) checked @endif id="financials">
                    <label class="custom-control-label" for="financials">Financials</label>
                  </div>
                </div>

                <div class="col-md-6 mt-3">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" @if($files) checked @endif id="files">
                    <label class="custom-control-label" for="files">Files</label>
                  </div>
                </div>


                <div class="col-md-6 mt-3">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" @if($testimonials) checked @endif id="testimonials">
                    <label class="custom-control-label" for="testimonials">Testimonials</label>
                  </div>
                </div>

                <div class="col-md-6 mt-3">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" @if($home) checked @endif id="home">
                    <label class="custom-control-label" for="home">Home</label>
                  </div>
                </div>

                <div class="col-md-6 mt-3">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" @if($reminders) checked @endif id="reminders">
                    <label class="custom-control-label" for="reminders">Reminders</label>
                  </div>
                </div>
                
            </div>
        </div>
        <div class="col-md-6"></div>
      </div>
</div>
@endsection

@section('scripts')
      <script>
          $('#files').click(function(){
            if($("#files").is(':checked'))
                addPermission('files');
            else
                removePermission('files');
          });

          $('#sales').click(function(){
            if($("#sales").is(':checked'))
                addPermission('sales');
            else
                removePermission('sales');
          });

          $('#orders').click(function(){
            if($("#orders").is(':checked'))
                addPermission('orders');
            else
                removePermission('orders');
          });

          $('#products').click(function(){
            if($("#products").is(':checked'))
                addPermission('products');
            else
                removePermission('products');
          });

          $('#staff').click(function(){
            if($("#staff").is(':checked'))
                addPermission('staff');
            else
                removePermission('staff');
          });

          $('#financials').click(function(){
            if($("#financials").is(':checked'))
                addPermission('financials');
            else
                removePermission('financials');
          });

          $('#testimonials').click(function(){
            if($("#testimonials").is(':checked'))
                addPermission('testimonials');
            else
                removePermission('testimonials');
          });

          $('#home').click(function(){
            if($("#home").is(':checked'))
                addPermission('home');
            else
                removePermission('home');
          });

          $('#reminders').click(function(){
            if($("#reminders").is(':checked'))
                addPermission('reminders');
            else
                removePermission('reminders');
          });

          function addPermission(permission){
              sendRequest(permission)
          }

          function removePermission(permission){
              removeRequest(permission)
          }

          function sendRequest(permission){
            $.ajax({
                    headers:
                    { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    url: "{{url('addPermission')}}/"+{{$id}}+"/"+permission,
                    method: 'GET',
                    success: function(data) {
                        location.reload();
                    }
                });
          }

          function removeRequest(permission){
            $.ajax({
                    headers:
                    { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    url: "{{url('removePermission')}}/"+{{$id}}+"/"+permission,
                    method: 'GET',
                    success: function(data) {
                        location.reload();
                    }
                });
          }
      </script>
@endsection
