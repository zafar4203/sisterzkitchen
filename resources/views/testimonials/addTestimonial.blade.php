@extends('layouts.main')

@section('title' , 'Add Testimonial')

@section('styles')
    <style>
      .add-testimonial-form{
        margin:0px 20%;
      }
      .error{
        color:red;
        font-size:1rem;
      }
      .submit-btn{
        margin-top:20px; 
        margin-bottom:20px; 
      }
      #grouped_items{
        display:none;
      }
      @media screen and (max-width: 600px) {
        .add-testimonial-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">
<form  class="add-testimonial-form" enctype="multipart/form-data"
 action="{{url('addTestimonial')}}" method="post">
@csrf

  <div class="form-group">
    <label for="productInput">Testimonial Title</label>
    <input type="text" name="title" class="form-control" value="{{old('title')}}" aria-describedby="titleHelp" placeholder="Enter Testimonial Title">
    @if($errors->has('title'))
    <small id="titleHelp" class="form-text error">{{ $errors->first('title') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="expenseDescription">Testimonial Description</label>
    <textarea type="text" rows="5" name="description" class="form-control" {{old('description')}} id="expenseDescription" aria-describedby="testimonialDescription" placeholder="Enter testimonial description">{{ old('description') }}</textarea>
    @if($errors->has('description'))
    <small id="testimonialDescription" class="form-text error">{{ $errors->first('description') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productInput">Testimonial Author</label>
    <input type="text" name="author" class="form-control" value="{{old('title')}}" aria-describedby="authorHelp" placeholder="Enter Testimonial Author">
    @if($errors->has('author'))
    <small id="authorHelp" class="form-text error">{{ $errors->first('author') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="testimonialDate">Testimonial Date</label>
    <input type="date" rows="5" name="date" class="form-control" value="{{old('date')}}" id="expenseDate" aria-describedby="testimonialDate" placeholder="Enter testimonial date">
    @if($errors->has('date'))
    <small id="testimonialDate" class="form-text error">{{ $errors->first('date') }}</small>
    @endif
  </div>

  <button id="submit" class="btn btn-primary submit-btn">Submit</button>
</form>

</div>
@endsection

@section('scripts')
@endsection