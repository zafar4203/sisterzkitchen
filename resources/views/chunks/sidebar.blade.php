    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-danger sidebar sidebar-dark accordion toggled" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('home') }}">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Sales</div>
      </a>

      
    <!-- Divider -->
    <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item {{ Request::is('home') ? 'active' : '' }}">
        <a class="nav-link" href="{{url('home')}}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

     <!-- Divider -->
     <hr class="sidebar-divider d-none d-md-block">

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item {{ Request::is('orders') ? 'active' : '' }}">
      <a class="nav-link collapsed" href="{{url('orders')}}">
        <i class="fas fa-fw fa-motorcycle"></i>
        <span>Home Delivery</span>
      </a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

        <li class="nav-item {{ Request::is('products') ? 'active' : '' }}"> 
          <a class="nav-link" href="{{url('products')}}">
            <i class="fas fa-fw fa-cart-plus"></i>
            <span>Products</span>
          </a>
        </li>

         <!-- Divider -->
    <hr class="sidebar-divider">
     <li class="nav-item {{ Request::is('staff') ? 'active' : '' }}"> 
       <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapseUsers" aria-controls="collapsePages" aria-expanded="false">
       <i class="fas fa-fw fa-user"></i>
         <span>Users</span>
       </a>
       <div id="collapseUsers" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar" style="">
         <div class="bg-white py-2 collapse-inner rounded">
           <a class="collapse-item" href="{{ url('staff') }}">Staff</a>
           <a class="collapse-item" href="{{ url('roles') }}">Role</a>
           <a class="collapse-item" href="{{ url('clients') }}">Client</a>
         </div>
       </div>
     </li>


        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item {{ Request::is('sales') ? 'active' : '' }}">
        <a class="nav-link collapsed" href="{{url('sales')}}">
          <i class="fas fa-fw fa-file"></i>
          <span>Sales</span>
        </a>
        </li>

    <!-- Divider -->
    <hr class="sidebar-divider">
     <li class="nav-item {{ Request::is('financials') ? 'active' : '' }}"> 
       <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapseFinancials" aria-controls="collapsePages" aria-expanded="false">
       <i class="fas fa-fw fa-file-invoice-dollar"></i>
         <span>Financials</span>
       </a>
       <div id="collapseFinancials" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar" style="">
         <div class="bg-white py-2 collapse-inner rounded">
         <a class="collapse-item" href="{{ url('financials') }}">Financial</a>
           <a class="collapse-item" href="{{ url('expenses') }}">Expenses</a>
           <a class="collapse-item" href="{{ url('revenues') }}">Revenues</a>
         </div>
       </div>
     </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item {{ Request::is('files') ? 'active' : '' }}">
    <a class="nav-link collapsed" href="{{url('files')}}">
      <i class="fas fa-fw fa-folder"></i>
      <span>Files</span>
    </a>
    </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item {{ Request::is('testimonials') ? 'active' : '' }}">
      <a class="nav-link collapsed" href="{{url('testimonials')}}">
        <i class="fas fa-fw fa-comment"></i>
        <span>Testimonials</span>
      </a>
      </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item {{ Request::is('reminders') ? 'active' : '' }}">
        <a class="nav-link collapsed" href="{{url('reminders')}}">
          <i class="fas fa-fw fa-clock"></i>
          <span>Reminders</span>
        </a>
        </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->