@extends('layouts.main')

@section('title' , 'Add Reminder')

@section('styles')
    <style>
      .edit-reminder-form{
        margin:0px 20%;
      }
      .error{
        color:red;
        font-size:1rem;
      }
      .submit-btn{
        margin-top:20px; 
        margin-bottom:20px; 
      }
      #grouped_items{
        display:none;
      }
      @media screen and (max-width: 600px) {
        .edit-reminder-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">
<form  class="edit-reminder-form" enctype="multipart/form-data"
 action="{{url('editReminder')}}" method="post">
@csrf
  <input type="hidden" name="id" value="{{ $reminder->id }}">
  <div class="form-group">
    <label for="productInput">Reminder Title</label>
    <input type="text" name="title" class="form-control" value="{{$reminder->title}}" aria-describedby="titleHelp" placeholder="Enter Reminder Title">
    @if($errors->has('title'))
    <small id="titleHelp" class="form-text error">{{ $errors->first('title') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productInput">Reminder Amount</label>
    <input type="text" name="amount" class="form-control" value="{{$reminder->amount}}" aria-describedby="amountHelp" placeholder="Enter reminder Amount">
    @if($errors->has('amount'))
    <small id="amountHelp" class="form-text error">{{ $errors->first('amount') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="reminderDescription">Reminder Description</label>
    <textarea type="text" rows="5" name="description" class="form-control" {{old('description')}} id="reminderDescription" aria-describedby="reminderDescription" placeholder="Enter Reminder description">{{ $reminder->description }}</textarea>
    @if($errors->has('description'))
    <small id="reminderDescription" class="form-text error">{{ $errors->first('description') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="submissionDate">Submission Date</label>
    <input type="date" rows="5" name="submit_date" class="form-control" value="{{ Carbon\Carbon::parse($reminder->submit_date)->format('Y-m-d') }}" id="submissionDate" aria-describedby="submissionDate" placeholder="Enter Submission date">
    @if($errors->has('submit_date'))
    <small id="submissionDate" class="form-text error">{{ $errors->first('submit_date') }}</small>
    @endif
  </div>

  <div class="form-group mt-4">
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio2" value="0" name="status" @if($reminder->status == 0) checked @endif>
        <label class="custom-control-label" for="customRadio2">Active</label>
    </div>

    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio1" value="1" name="status" @if($reminder->status == 1) checked @endif>
        <label class="custom-control-label" for="customRadio1">DeActive</label>
      </div>
  </div>

  <button id="submit" class="btn btn-primary submit-btn">Submit</button>
</form>

</div>
@endsection

@section('scripts')
@endsection