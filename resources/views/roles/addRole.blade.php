@extends('layouts.main')

@section('title' , 'Add Role')

@section('styles')
    <style>
      .add-role-form{
        margin:0px 20%;
      }
    .error{
      color:red;
      font-size:1rem;
    }
    @media screen and (max-width: 600px) {
        .add-role-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">
<form  class="add-role-form" action="{{url('addRole')}}" id="add_role" method="post">
@csrf
  <div class="form-group">
    <label for="roleInput">Role Name</label>
    <input type="text" name="name" class="form-control" id="roleInput" aria-describedby="productNameHelp" value="{{ old('name') }}" placeholder="Enter Role Name">
    @if($errors->has('name'))
    <small id="roleNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>

  <button id="submit" class="btn btn-primary">Submit</button>
</form>

</div>
@endsection
