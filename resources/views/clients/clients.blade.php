@extends('layouts.main')

@section('title' , 'Clients')

@section('styles')
    <link href="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <style>
      .ml-00{
          margin-left:1px !important;
      }
        .text-primary{
            color:#e74a3b !important;            
        }    
        .h3, h3 {
            font-size: 1.05rem;
        }
        .checked {
        color: orange;
        }
    </style>
@endsection

@section('content')
<div class="container-fluid">
      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800">Clients

          <!-- DataTales Example -->
          <div class="card shadow mt-2 mb-4">
            <div class="card-body">
              
              <div class="table-responsive">
                <table class="table table-bordered" id="clientTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                        <th>Stars</th>
                        <th>Orders</th>
                        <th>Expend</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <th>Stars</th>
                        <th>Orders</th>
                        <th>Expend</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

@endsection

@section('scripts')
  <!-- Page level plugins -->
  <script src="{{asset('public/vendor/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

  <!-- Page level custom scripts -->
  <script src="{{asset('public/js/demo/datatables-demo.js')}}"></script>

  <script>
     $("#alert").fadeTo(2000, 500).slideUp(500, function(){
       $("#alert").slideUp(600);
       });


        $('#clientTable').DataTable({
              "processing":true,
              "serverside":true,
              "bPaginate": false,
              "bInfo" : false,
              "pageLength": 50,
              "ajax":"{{url('ajaxClients')}}",
              "columns":[
                {"data" : "stars"},
                {"data" : "orders"},
                {"data" : "total"},
                {"data" : "name"},
                {"data" : "phone"},
                {"data" : "email"},
              ],
              order:[],
            });

  </script>
@endsection