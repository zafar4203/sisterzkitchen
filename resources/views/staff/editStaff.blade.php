@extends('layouts.main')

@section('title' , 'Edit Staff')

@section('styles')
    <style>
      .add-staff-form{
        margin:0px 20%;
      }
      .error{
        color:red;
        font-size:1rem;
      }
      .submit-btn{
        margin-top:20px; 
        margin-bottom:20px; 
      }
      #grouped_items{
        display:none;
      }
      @media screen and (max-width: 600px) {
        .add-staff-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">
<form  class="add-staff-form" enctype="multipart/form-data"
 action="{{url('editStaff')}}" method="post">
@csrf

  <input type="hidden" name="id" value="{{$staff->id}}" />

  <div class="form-group">
    <label for="productInput">Name</label>
    <input type="text" name="name" class="form-control" value="{{ $staff->name }}" aria-describedby="nameHelp" placeholder="Enter Name">
    @if($errors->has('name'))
    <small id="nameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productInput">Email</label>
    <input type="text" name="email" class="form-control" value="{{$staff->email}}" aria-describedby="emailHelp" placeholder="Enter Email">
    @if($errors->has('email'))
    <small id="emailHelp" class="form-text error">{{ $errors->first('email') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productInput">Password</label>
    <input type="text" name="password" class="form-control"  aria-describedby="passwordHelp" placeholder="Enter Password">
    @if($errors->has('password'))
    <small id="passwordHelp" class="form-text error">{{ $errors->first('password') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productInput">Mobile</label>
    <input type="text" name="mobile" class="form-control" value="{{ $staff->mobile }}" aria-describedby="mobileHelp" placeholder="Enter Mobile">
    @if($errors->has('mobile'))
    <small id="mobileHelp" class="form-text error">{{ $errors->first('mobile') }}</small>
    @endif
  </div>

  <div class="form-group">
  <label for="genderInput">Gender</label>
  <select name="gender" class="form-control" id="">
    <option value="Male">Male</option>
    <option value="Female">Female</option>
  </select>
  @if($errors->has('gender'))
  <small class="form-text error">{{ $errors->first('gender') }}</small>
  @endif
</div>


  <div class="form-group">
    <label for="productInput">Age</label>
    <input type="text" name="age" class="form-control" value="{{$staff->age}}" aria-describedby="ageHelp" placeholder="Enter Testimonial Author">
    @if($errors->has('age'))
    <small id="ageHelp" class="form-text error">{{ $errors->first('age') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productInput">Cnic</label>
    <input type="text" name="cnic" class="form-control" value="{{$staff->cnic}}" aria-describedby="cnicHelp" placeholder="Enter Cnic">
    @if($errors->has('cnic'))
    <small id="cnicHelp" class="form-text error">{{ $errors->first('cnic') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="address">Expense Address</label>
    <textarea type="text" rows="5" name="address" class="form-control" {{old('description')}} id="expenseDescription" aria-describedby="addressHelp" placeholder="Enter Address">{{ $staff->address }}</textarea>
    @if($errors->has('address'))
    <small id="addressHelp" class="form-text error">{{ $errors->first('address') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="userRole">Role</label>
    <select name="role" class="form-control" id="userRole">
      <option value="" disable>Select Role Please</option>
      @foreach($roles as $role)
      <option value="{{ $role->id }}" @if($role->id ==  $staff->role) selected @endif> {{ $role->name }} </option>
      @endforeach
    </select>
    @if($errors->has('role'))
      <small id="userRoleHelp" class="form-text error">{{ $errors->first('role') }}</small>
    @endif
    </div>

  <div class="form-group">
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio2" value="0" name="block" @if($staff->block == 0) checked @endif>
        <label class="custom-control-label" for="customRadio2">Approve</label>
    </div>

    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio1" value="1" name="block" @if($staff->block == 1) checked @endif>
        <label class="custom-control-label" for="customRadio1">Block</label>
      </div>
  </div>

  <button id="submit" class="btn btn-primary submit-btn">Submit</button>
</form>

</div>
@endsection

@section('scripts')
@endsection