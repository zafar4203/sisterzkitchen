@extends('layouts.main')

@section('title' , 'Edit Expense')

@section('styles')
    <style>
      .add-expense-form{
        margin:0px 20%;
      }
      .error{
        color:red;
        font-size:1rem;
      }
      .submit-btn{
        margin-top:20px; 
        margin-bottom:20px; 
      }
      #grouped_items{
        display:none;
      }
      @media screen and (max-width: 600px) {
        .add-expense-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">

<form  class="add-expense-form" enctype="multipart/form-data"
 action="{{url('editExpense')}}" id="add_expense" method="post">
@csrf

@if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif

<input type="hidden" name="id" value="{{$expense->id}}" />
  <div class="form-group">
    <label for="productInput">Expense Name</label>
    <input type="text" name="name" class="form-control" value="{{$expense->name}}" aria-describedby="expenseNameHelp" placeholder="Enter expense name">
    @if($errors->has('name'))
    <small id="expenseNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>


  <div class="form-group">
    <label for="expenseAmount">Expense Amount</label>
    <input type="text" name="amount" class="form-control" value="{{$expense->amount}}" id="expenseAmount" aria-describedby="expenseAmountHelp" placeholder="Enter expense amount">
    @if($errors->has('amount'))
    <small id="expenseAmountHelp" class="form-text error">{{ $errors->first('amount') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="expenseDescription">Expense Description</label>
    <textarea type="text" rows="5" name="description" class="form-control" id="expenseDescription" aria-describedby="expenseDescriptionHelp" placeholder="Enter expense description">{{ $expense->description }}</textarea>
    @if($errors->has('description'))
    <small id="expenseDescriptionHelp" class="form-text error">{{ $errors->first('description') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="expenseDate">Expense Date</label>
    <input type="date" rows="5" name="date" class="form-control" value="{{ Carbon\Carbon::parse($expense->date)->format('Y-m-d') }}" id="expenseDate" aria-describedby="expenseDateHelp" placeholder="Enter expense date">
    @if($errors->has('date'))
    <small id="expenseDateHelp" class="form-text error">{{ $errors->first('date') }}</small>
    @endif
  </div>

  <button id="submit" class="btn btn-primary submit-btn">Submit</button>
</form>

</div>
@endsection

@section('scripts')
@endsection