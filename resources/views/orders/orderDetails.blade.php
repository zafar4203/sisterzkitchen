@extends('layouts.main')

@section('title' , 'Order Details')

@section('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.standalone.min.css" rel="stylesheet"/>

    <style>
      .error{
        color:red;
        font-size:1rem;
      }
      .item-image{
            width:50px;
            display: block;
            margin-left: auto;
            margin-right: auto;
      }
      .item-big-image{
            width:100%;
            display: block;
            margin-left: auto;
            margin-right: auto;
      }
      .bg-white{
          background:white;
      }
      .highlight{
          background:#F7E771;
          color:black;
          padding-right:5px;
      }
      .highlight-green{
          background:#cdffcc;
          color:black;
          padding-right:5px;
      }
      .order-status{
          font-size:18px;
          color:black;
          padding:5px;
          cursor:pointer;
      }
      .new{
          background:#feffb3;
      }
      .on-way{
          background:#F2C481;
      }
      .delivered{
          background:#cdffcc;
      }
      .canceled{
          background:#ffb3b3;
      }
      .edit{
        background:#13b3b3;
      }
      .btn-block{
          color:black;
      }
      .edit-btn , #userAddressSpan , #deliveryText{
        cursor:pointer;
      }

/* add minus buttons */
.qty .count {
    color: #000;
    display: inline-block;
    vertical-align: top;
    font-size: 25px;
    font-weight: 700;
    line-height: 30px;
    padding: 0 2px
    ;min-width: 35px;
    text-align: center;
}
.qty .plus {
    cursor: pointer;
    display: inline-block;
    vertical-align: top;
    color: white;
    width: 30px;
    height: 30px;
    font: 30px/1 Arial,sans-serif;
    text-align: center;
    border-radius: 50%;
    }
.qty .minus {
    cursor: pointer;
    display: inline-block;
    vertical-align: top;
    color: white;
    width: 30px;
    height: 30px;
    font: 30px/1 Arial,sans-serif;
    text-align: center;
    border-radius: 50%;
    background-clip: padding-box;
}
#icons {
    text-align: center;
}
.minus:hover{
    background-color: #717fe0 !important;
}
.plus:hover{
    background-color: #717fe0 !important;
}
/*Prevent text selection*/
span{
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
}
input{  
    border: 0;
    width: 2%;
}
input.count::-webkit-outer-spin-button,
input.count::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
input:disabled{
    background-color:white;
}
.big-text{
    font-size:16px;
    color:#000000;
    font-weight:bold;
}


.border{
    padding-bottom:10px;
}
#addItemsModal .modal-header,
#addItemsModal .modal-footer{
    border:0px;
}

#cancelCommentBox{
    display:none;
}

@media screen and (max-width: 600px) {
        .text-gray-800{
            font-size:20px;
        }
    }
    </style>
@endsection

@section('content')
<div class="container-fluid">

@if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Order Details <span id="order_status" onClick="statusModal()" class="float-right order-status {{\App\Order::statusClass($order->order_status)}}">{{ \App\Order::orderStatus($order->order_status)}}</span></h1>

    <div class="card shadow mt-3 mb-4">
    <div class="card-body">
      <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4"><b>Name</b></div>
                    <div class="col-md-8">{{ $order->user_name }}</div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Phone</b></div>
                    <div class="col-md-8"><span class="highlight">{{ $order->user_phone }}</span></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Email</b></div>
                    <div class="col-md-8"><span class="highlight">{{ $order->user_email }}</span></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Address</b></div>
                    <div @if($order->order_status != "2") onClick="changeAddress()" @endif class="col-md-8"><span class="highlight" id="userAddressSpan">{{ $order->user_address }}</span></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4"><b>Order #</b></div>
                    <div class="col-md-8"><span class="highlight">{{ $order->id }}</span></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Sub Total</b></div>
                    <div class="col-md-8">Rs {{ (int)$order->total_price  + (int)$order->discount  - (int)$order->delivery}}</div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Delivery Price</b></div>
                    <div class="col-md-8">Rs {{ $order->delivery }}</div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Discount Price</b></div>
                    <div class="col-md-8">Rs {{ $order->discount }}</div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Total Price</b></div>
                    <div class="col-md-8"><span class="highlight-green">Rs {{$order->total_price }}</span></div>
                </div>
            </div>
      </div>

      <br><br>
      <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4"><b>Order Items</b></div>
                    <div class="col-md-8">{{ $order->total_items }}</div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Order Placed</b></div>
                    <div class="col-md-8"><span class="highlight">{{ $order->created_at->format('j F Y') }}</span></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4"><b>Order Medium</b></div>
                    <div class="col-md-8"><span>@if($order->order_from == "WEB") Online @else Phone Order @endif</b></span></div>
                </div>

            </div>
      </div>
      </div>
      </div>
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Order Items Details <span class="float-right"><button id="add_btn" class="btn text-primary btn-default"><b>Add Items</b></button></span></h1>

    <!-- DataTales Example -->
    <div class="card shadow mt-2 mb-4">
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered text-center" id="orderTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Item #</th>
              <th>Image</th>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Total</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($orders as $od)
            <tr>
                <th>{{$od->id}}</th>
                <th><img class="item-image" src="{{$od->product->image}}" alt=""></th>
                <th>{{$od->product_name}}</th>
                <th>{{$od->product_price}}</th>
                <th>{{$od->product_quantity}}</th>
                <th>{{$od->product_price * $od->product_quantity}}</th>
                <th><a data-toggle="modal" data-target="#quantityModal" class="text-primary" onClick="openModal({{$od->id}} , {{$od->product_quantity}})"><span><i class="fa fa-edit"></i></span></a>
                <a class="edit-btn text-danger" onClick="delete_click({{$od->id}})"><span><i class="fa fa-trash"></i></span></a>
                </th>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>


  <!-- Status Modal -->
<div class="modal" id="statusModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Change Order Status</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
            <button id="0" class="btn change-status btn-block new btn-default">New Order</button>
            <button id="1" class="btn change-status btn-block on-way btn-default">On Way</button>
            <button id="2" class="btn change-status btn-block delivered btn-default" id="delivered">Delivered</button>
            <button id="3" class="btn change-status btn-block canceled btn-default">Cancel</button>
      </div>

      <div id="cancelCommentBox" class="col-md-12 pb-2">
          <textarea class="form-control mb-2" id="cancel_comments" cols="20" rows="3" placeholder="Cancel Comments"></textarea>
          <button id="cancelSubmitBtn" class="btn btn-primary float-right">Submit</button>
      </div>
    </div>
  </div>
</div>


<!-- Quantity Modal -->
  <!-- The Modal -->
  <div class="modal" id="quantityModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Change Order Quantity</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
            <input min="1" class="form-control" id="order_quantity" type="number"  >
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" id="submit" class="btn btn-success" data-dismiss="modal">Submit</button>
      </div>

    </div>
  </div>
</div>


<!-- Address Modal -->
  <!-- The Modal -->
  <div class="modal" id="addressModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Change User Address</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
            <input class="form-control" id="user_address" type="text">
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" id="submitAddress" class="btn btn-success">Submit</button>
      </div>

    </div>
  </div>
</div>

    <!-- Delete Modal-->
    <div class="modal fade" id="deleteProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Are you sure to delete?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select <span class="error">"Delete"</span> below if you are ready to delete the product.</div>
        <div class="modal-footer">
          <button class="btn btn-success" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" id="deleteModel" href="#">Delete</a>
        </div>
      </div>
    </div>
  </div>



    <!-- Add Items Model -->
      <!-- The Modal -->
  <div class="modal fade" id="addItemsModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Add Items</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <div id="items_row" class="row">
            </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" id="add_items_btn" class="btn btn-secondary">Add</button>
        </div>
        
      </div>
    </div>
  </div>

  <!-- Delivery Modal -->
  <!-- The Modal -->
  <div class="modal" id="deliveryModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Change Delivery Time</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="form-group">
            <label >Select Date Please</label>
            <input type="text" class="form-control" id="datepicker" placeholder="MM/DD/YYYY">
        </div>

        <div class="form-group">
                <label >Select Time Please</label>
                <select id="time" class="form-control" name="" id="">
                    <option value="">Select Time</option>
                </select>
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" id="submitDelivery" class="btn btn-success">Submit</button>
      </div>

    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>

  <script>
  var order_id = "";
  var order_quant = "";
  var products = "";
  var cancelCheck = false;

     $("#alert").fadeTo(2000, 500).slideUp(500, function(){
       $("#alert").slideUp(600);
       });

       function delete_click(clicked_id){
          $('#deleteModel').attr("href","{{url('deleteOrderItem')}}/"+{{$order->id}}+"/"+clicked_id);
          $('#deleteProductModal').modal('show');
        }

       function openModal(val , quantity){
            order_id = val;
            order_quant = quantity;
            $('#order_quantity').val(quantity);
       }

       function statusModal(){
            var status = {{ $order->order_status }} + "";
            if(status != "2"){
                $('#statusModal').modal("show");           
            }
       }

       $('#submit').click(function(){
           if($('#order_quantity').val() == 0){
                alert("Enter Suitable Quantity");            
           }
           if($('#order_quantity').val() == order_quant){
              $('#quantityModal').modal("hide"); 
           }
           else{
              
            $.ajax({
                    headers:
                    { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    url: "{{url('orderChange')}}",
                    method: 'POST',
                    data:{
                        'id' : {{$order->id}},
                        'item_id' : order_id,
                        'quantity' : $('#order_quantity').val(),
                    },
                    success: function(data) {
                        if(data){               
                            location.reload();
                        } 
                    }
                });


            }
       });

        // Change Order Status

       $('.change-status').click(function(){
        var thiss = $('#order_status');
        var status = $(this).attr('id');

        if(status == "3"){
            $('#cancelCommentBox').css('display' , 'block');
        }else{
            $.ajax({
                headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                url: "{{url('orderStatus')}}/"+{{$order->id}}+"/"+status,
                method: 'GET',
                success: function(data) {
                    if(data){               
                        location.reload();
                    }
                } 
            });
        }
    });


    $('#cancelSubmitBtn').click(function(){
        $.ajax({
            headers:
            { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: "{{url('api/orderCancel')}}",
            method: 'POST',
            data:{
                'user_id' : {{ Auth::id() }},
                'order_id' : {{ $order->id }},
                'cancel_comments' : $('#cancel_comments').val(),
            },
            success: function(data) {

                    $.ajax({
                        headers:
                        { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        url: "{{url('orderStatus')}}/"+{{$order->id}}+"/3",
                        method: 'GET',
                        success: function(data) {
                            if(data){               
                                location.reload();
                            }
                        } 
                    });
            
            }
        });
    });

    // Add Order Items to Existing Order

    $('#add_btn').click(function(){
        $.ajax({
            headers:
            { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: "{{url('api/products')}}",
            method: 'GET',
            success: function(data) {
                if(data){           
                    products = data;
                    var locations = [
                    @foreach ($orders as $ode)
                        [ "{{ $ode->product->id }}" ], 
                    @endforeach
                    ];
                    $('#items_row').html("");
                    data.forEach(function(product) {
                        var check = true;
                        locations.forEach(function(item){
                            if(item == product.id){
                                 check = false;
                             }                    
                        });
                        if(check){
                            var item = "<div class='col-md-3 border'>"+
                                    "<div class='col-md-12'>"+
                                        "<img class='item-big-image' src="+product.image+">"+
                                        "<div class='col-md-12 text-center big-text'>"+product.short_name+"</div>"+
                                        "<div class='col-md-12'>"+
                                            "<div id='icons' class='qty mt-1'>"+
                                                "<span id="+product.id+" onClick='removeProduct("+product.id+")' class='minus bg-primary'>-</span>"+
                                                "<input type='number' id='count"+product.id+"' class='count' name='qty' value='0'>"+
                                                "<span id="+product.id+" onClick='addProduct("+product.id+")' class='plus bg-primary'>+</span>"+
                                            "</div>"+
                                        "</div>"+
                                    "</div>"+
                                "</div>";
                                $('#items_row').append(item);
                            $('#addItemsModal').modal("show");
                        }

                    });
                }
            }
        });
    });

    $(document).ready(function(){
		    $('.count').prop('disabled', true);
   			$(document).on('click','.plus',function(){
				$('#count'+this.id).val(parseInt($('#count'+this.id).val()) + 1 );
    		});
        	$(document).on('click','.minus',function(){
    			$('#count'+this.id).val(parseInt($('#count'+this.id).val()) - 1 );
    				if ($('#count'+this.id).val() <= 0) {
						$('#count'+this.id).val(0);
					}
            });
 		});


        function addProduct(id){
            products.forEach(function(product){
                if(product.id == id){
                    var quant = parseInt($('#count'+id).val())+1;
                    if(quant > 0){
                        product.quantity = quant;
                    }else{
                        product.quantity = 0;
                    }
                    console.log(product);
                }
            });
        }

        function removeProduct(id){
            products.forEach(function(product){
                if(product.id == id){
                    var quant = parseInt($('#count'+id).val())-1;
                    if(quant > 0){
                        product.quantity = quant;
                    }else{
                        product.quantity = 0;
                    }
                    console.log(product);
                }
            });
       }

       $('#add_items_btn').click(function(){
           var quant = 0;
           products.forEach(function(product){
               if(product.quantity > 0){
                   quant ++;
               }
           }); 
           if(quant > 0){
                products.forEach(function(product){
                    if(product.quantity > 0){
                        $.ajax({
                            headers:
                            { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                            url: "{{url('api/orderItems')}}",
                            method: 'POST',
                            data:{
                                'order_id' : {{ $order->id }},
                                'product_id' : product.id,
                                'product_name' : product.name,
                                'product_price' : product.price,
                                'product_quantity' : product.quantity,
                            },
                            success: function(data) {
                                if(data){               
                                   console.log(data);
                                } 
                            }
                        });
                    }
                }); 
                updateOrder();
           }else{
               alert("Please add some products");
           }
       });

       function updateOrder(){
               $.ajax({
                    headers:
                    { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    url: "{{url('orderUpdate')}}/"+{{ $order->id }},
                    method: 'GET',
                    success: function(data) {
                        if(data){               
                            location.reload();
                        } 
                    }
                });
       }

    //    Update User Address Working
       function changeAddress(){
            $('#user_address').val($('#userAddressSpan').text());
            $('#addressModal').modal('show');
       }

       $('#submitAddress').click(function(){
            if($('#user_address').val() != ""){
                $.ajax({
                    headers:
                    { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    url: "{{url('orderUserAddress')}}",
                    method: 'POST',
                    data:{
                        order_id : {{ $order->id }},
                        user_address : $('#user_address').val()
                    },
                    success: function(data) {
                        if(data){               
                            $('#userAddressSpan').text(data.user_address);
                            $('#addressModal').modal('hide');
                        } 
                    }
                });
            }else{
                alert("Please type Address");
            }
       });

  </script>
@endsection