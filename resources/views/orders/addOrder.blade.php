@extends('layouts.main')

@section('title' , 'Add Order')

@section('styles')
    <style>
      .item-big-image{
            width:100%;
            display: block;
            margin-left: auto;
            margin-right: auto;
      }

/* add minus buttons */
.qty .count {
    color: #000;
    display: inline-block;
    vertical-align: top;
    font-size: 25px;
    font-weight: 700;
    line-height: 30px;
    padding: 0 2px;
	min-width: 35px;
    text-align: center;
}
.qty .plus {
    cursor: pointer;
    display: inline-block;
    vertical-align: top;
    color: white;
    width: 30px;
    height: 30px;
    font: 30px/1 Arial,sans-serif;
    text-align: center;
    /* border-radius: 50%; */
    }
.qty .minus {
    cursor: pointer;
    display: inline-block;
    vertical-align: top;
    color: white;
    width: 30px;
    height: 30px;
    font: 30px/1 Arial,sans-serif;
    text-align: center;
    /* border-radius: 50%; */
    background-clip: padding-box;
}
#icons {
    text-align: center;
}
.minus:hover{
    background-color: #717fe0 !important;
}
.plus:hover{
    background-color: #717fe0 !important;
}
/*Prevent text selection*/
span{
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
}
input{  
    border: 0;
    width: 2%;
}
input.count::-webkit-outer-spin-button,
input.count::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
input:disabled{
    background-color:white;
}
.big-text{
    font-size:16px;
    color:#000000;
    font-weight:bold;
}


.border{
    padding-bottom:10px;
    padding-top:10px;
    background:white;
}
.total-box{
    background:#ffffff!important;
    color:#000000;
}
.total-box .row{
    padding:10px 0px;
    border-bottom:2px solid #e74a3b;
}

.error{
    font-size:14px;
    color:red;
}
.custom-control-label{
    font-size:14px;
}
.loader{
	width:100%;
	height:auto;
}


@media screen and (max-width: 600px) {
        .p-mobile-0{
          padding-right:0px !important;
		  padding-left:0px !important;
        }
      }
    </style>
@endsection

@section('content')
    <div class="container-fluid" id="app">
        <div class="row">
        <div class="col-md-8">
            <div class="row">
    
                <div v-for="product in products" :key="product" class='col-md-4 col-6 border'>                    
                    <div class='col-md-12'>
                        <!-- <img class='item-big-image' :src="product.image"> -->
                        <div class='col-md-12 text-center big-text'>@{{product.short_name}}</div>
                            <div class='p-mobile-0 col-md-12'>
                                <div id='icons' class='qty mt-1'>
                                    <span @click="decrement(product)" class='minus bg-danger'>-</span>
                                    <input type='number' class='count' name='qty' :value="product.quantity">
                                    <span @click="increment(product)" class='plus bg-danger'>+</span>
                                </div>
	                            </div>
                        </div>
                    </div>                    
                </div>

            <div class="row mb-5 ">
                <div class="col-md-5"></div>
                <div class="col-md-6 mt-5 total-box">
                    <div class="row">
                        <div class="col-md-6">Sub Total</div>
                        <div class="col-md-6 text-right">Rs @{{subtotal}}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">Delivery Fee</div>
                        <div class="col-md-6 text-right">Rs @{{deliveryFee}}</div>
                    </div>
					<div class="row">
                        <div class="col-md-6">Discount</div>
                        <div class="col-md-6 text-right">Rs @{{discount}}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">Total Order</div>
                        <div class="col-md-6 text-right">Rs @{{total}}</div>
                    </div>
					<div v-if="recieved_amount != 0" class="row">
                        <div class="col-md-6">Recieved Amount</div>
                        <div class="col-md-6 text-right">Rs @{{recieved_amount}}</div>
                    </div>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
        <div class="col-md-4">
                    <form class="ps-form--checkout" @submit.prevent="onSubmit">
								<div class="form-group">
									<input class="form-control" type="text" name="user_name" v-model="user_name" v-validate="'required|alpha_spaces|max:15'" data-vv-as="Name" placeholder="Name">
									<div class="error" v-if="errors.has('user_name')">@{{errors.first('user_name')}}</div>
								</div>
								<div class="form-group">
									<input class="form-control" type="text" name="user_phone" v-model="user_phone" v-validate="'required|digits:11'" data-vv-as="Mobile Number" placeholder="Mobile">
									<div class="error" v-if="errors.has('user_phone')">@{{errors.first('user_phone')}}</div>
								</div>
								<div class="form-group">
									<input class="form-control" type="email" name="user_email" v-model="user_email" v-validate="'email'" data-vv-as="Email Address" placeholder="Email">
									<div class="error" v-if="errors.has('user_email')">@{{errors.first('user_email')}}</div>
								</div>
								<div class="form-group">
									<input class="form-control" type="text" name="user_address" v-model="user_address" v-validate="'required|min:5|max:80'" data-vv-as="Address" placeholder="Address">
									<div class="error" v-if="errors.has('user_address')">@{{errors.first('user_address')}}</div>
								</div>
								<label for="">Order Date</label>
								<div class="form-group">
									<input class="form-control" type="date" name="order_date" v-model="order_date" v-validate="'required'" data-vv-as="Date" placeholder="Order Date">
									<div class="error" v-if="errors.has('order_date')">@{{errors.first('order_date')}}</div>
								</div>
								<label for="">Recieved Amount</label>
								<div class="form-group">
									<input class="form-control" type="number" name="recieved_amount" v-model="recieved_amount" v-validate="'required'" data-vv-as="Recieved Amount" placeholder="Recieved Amount">
									<div class="error" v-if="errors.has('recieved_amount')">@{{errors.first('recieved_amount')}}</div>
								</div>
								<label for="">Delivery Fee</label>
								<div class="form-group">
									<input class="form-control" type="number" name="deliveryFee" v-model="deliveryFee" v-validate="'required'" data-vv-as="Delivery Fee" placeholder="Delivery Fee">
									<div class="error" v-if="errors.has('deliveryFee')">@{{errors.first('deliveryFee')}}</div>
								</div>

								<button v-if="!orderPlaceCheck" type="submit" class="btn btn-danger btn-block"><span>Place Order</span></button>
								<img v-if="orderPlaceCheck" class="loader" src="{{asset('public/images/loader.gif')}}" alt="">

								<div v-if="orderPlacedCheck" class="alert alert-success mt-2 alert-dismissible">
									<button type="button" class="close" data-dismiss="alert">&times;</button>
									<strong>Congratz!</strong> Your Order is Placed.
								</div>
							</form>
        </div>

        </div>
    </div>
@endsection

@section('scripts')
		<script src="https://cdn.jsdelivr.net/npm/vue"></script>
		<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/vee-validate/2.2.15/vee-validate.min.js"></script>

		<script>
			Vue.use(VeeValidate); 
			new Vue({
				el: '#app',
				data: {
					deliveryFee : 120,
					products: [] ,
					user_name:'',
					user_email:'',
					user_phone:'',
					recieved_amount:0,
					order_date:'',
					user_phone:'',
					user_address:'',
					orderPlaceCheck:false,
					load:false,
					orderPlacedCheck:false,
				},
				mounted() {
					var vm = this;
					axios.get("{{ url('api/productsAdm') }}")
					.then(response => {				
						response.data.forEach((product) => {
							product.quantity = 0;
							vm.products.push(product);
							vm.load = true;
						});		
						console.log(this.products);
					})
				},
				computed:{
					subtotal:function(){
						let total = 0;
						this.products.forEach((product) => {
							total = total + (product.quantity * product.price)
						});	
						if(total == 0){
							this.deliveryFee = 0;
						}	
						else if(total >= 1199){
							this.deliveryFee = 0;
						}else{
							this.deliveryFee = 120;
						}
						return total;
					},
					total:function(){
						return parseFloat(this.subtotal) + parseFloat(this.deliveryFee); 
					},
					discount:function(){
						var discount = 0;
						if(this.recieved_amount != 0){
							if(parseFloat(this.recieved_amount) < parseFloat(this.total)){
								return this.total - this.recieved_amount;
							}	
						}
						return 0;
					},
					total_items:function(){
						var count = 0;
						this.products.forEach((product) => {
							if(product.quantity > 0){
								count++;
							}
						});
						return count;
					}
				},
				methods: {
					increment:function (product){
						product.quantity = product.quantity + 0.5; 
					},
					decrement:function (product){
						if(product.quantity > 0){
							product.quantity = product.quantity - 0.5; 
						}
					},
					placeOrderItems : function(id){
						var vm = this;
						this.products.forEach((product) => {
							if(product.quantity > 0){
								axios({
								method: 'post',
								url: "{{ url('api/orderItems') }}",
								data: {
									order_id : id,
									product_id : product.id,
									product_name : product.name,
									product_price : product.price,
									product_quantity : product.quantity,
								}})
								.then(function (response) {
									console.log(response);
									vm.sendOrderEmails(id);
								})
								.catch(function (response) {
									console.log(response);
								});						
							}
						});

							setTimeout(() => {
								vm.orderPlaceCheck = false;
								vm.orderPlacedCheck=true;
								setTimeout(() => vm.orderPlacedCheck = false, 8000);

								vm.user_name = '';
								vm.user_email = '';
								vm.user_phone = '';
								vm.user_address = '';
								vm.order_date = '';
								vm.recieved_amount = '';
								vm.deliveryFee = '';
								vm.products.forEach((product) => {
									product.quantity = 0;	
								});	
								vm.$validator.reset();
							}, 4000);

					},
					sendOrderEmails : function(id){
						var vm = this;
						axios({
						method: 'post',
						url: "{{ url('api/sendOrderEmails') }}",
						data: {
							order_id : id,
							user_name : this.user_name,
							user_email : this.user_email,
							order_date : this.order_date
						}})
						.then(function (response) {
						})
						.catch(function (response) {
							//handle error
							console.log(response);
						});						
					},
					onSubmit(){
						var vm = this;
						this.$validator.validateAll().then(result=> {
							if(result){
								if(this.subtotal>0){
										this.orderPlaceCheck = true;
										axios({
										method: 'post',
										url: "{{ url('api/placeOrder') }}",
										data: {
											user_name : this.user_name,
											user_email : this.user_email,
											user_phone : this.user_phone,
											user_address : this.user_address,
											order_from : "web",
											total_price : this.total,
											total_items : this.total_items,
											recieved_amount : this.recieved_amount,
											delivery_fee : this.deliveryFee,
											date : this.order_date,
											discount : this.discount,
										}})
										.then(function (response) {
											if(response.data.success){
												vm.placeOrderItems(response.data.order.id);	
											}
										})
										.catch(function (response) {
											//handle error
											console.log(response);
										});
								}else{
									alert("Please Add Some Product to buy");
								}
							}
						});
					}
				}
			})
		</script>
		@endsection