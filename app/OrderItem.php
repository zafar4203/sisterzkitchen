<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = [
        'order_id',
        'product_id',
        'product_name',
        'product_price',
        'product_quantity'
    ];

    public function product(){
        return $this->hasOne('App\Product', 'id', 'product_id');
    }
}
