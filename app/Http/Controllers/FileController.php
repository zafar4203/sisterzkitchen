<?php

namespace App\Http\Controllers;

use File;
use ZipArchive;
use Validator;
use App\Filu;
use Illuminate\Http\Request;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        // $this->middleware('auth:staff');
    }
    
    public function index()
    {
        $license_permits = 0;
        $leasing_contracts = 0;
        $hr = 0;
        $permits = 0;
        $operations = 0;
        $establishment = 0;
        $files = Filu::all();
        foreach($files as $file){
            if($file->category == "License Permits"){
                $license_permits++;
            }
            if($file->category == "Leasing Contracts"){
                $leasing_contracts++;
            }
            if($file->category == "HR"){
                $hr++;
            }
            if($file->category == "Permits"){
                $permits++;
            }
            if($file->category == "Operations"){
                $operations++;
            }
            if($file->category == "Establishment"){
                $establishment++;
            }
        }
        return view('files.files' , compact('files' , 'establishment' , 'operations' , 'permits' , 'hr' ,'leasing_contracts' , 'license_permits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('files.addFile');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'category' => 'required',
            'file_url' => 'required'
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $filu = Filu::create([
            'name' => $request->name , 
            'category' => $request->category , 
            'file_url' => '' 
             ]);
        $filu->save();

        $request->name = preg_replace('/\s+/', '_', $request->name);

        $fileUrl = "";
        $filename = "";
        if($request->hasfile('file_url')){
            $files = $request->file('file_url');
            for($i=0 ; $i<count($files) ; $i++){
                $img = "";
                $file = $files[$i];
                $filename = $request->name.'.'.$file->getClientOriginalExtension();
                $destinationPath = public_path('/files');
                $file->move($destinationPath, $filename);    
                $img = asset("public/files/".$filename);
    
                if($i == 0){
                    $fileUrl = asset("public/files/".$filename);
                }

            }
        }

        $fileUpdate = Filu::where(['id' => $filu->id])->first();
        $fileUpdate->name = $filename;
        $fileUpdate->file_url = $fileUrl;
        $fileUpdate->save();

        return redirect('files')->with('message', 'File Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $file = Filu::where(['id' => $id])->first();
        $destinationPath = public_path('/files')."/".$file->name;
        if(File::exists($destinationPath)) {
            File::delete($destinationPath);            
        }
        $file->delete();
        
        return redirect()->back()->with('message', 'File Deleted Successfully');;
    }


    public function zipFile($names){
        $names = explode(',', $names);

        $zipFileName = 'files.zip';
        $zip = new ZipArchive;
        if ($zip->open(public_path('/files') .'/'. $zipFileName, ZipArchive::CREATE) === TRUE) {
            // Add File in ZipArchive
            foreach($names as $name){
                $zip->addFile(public_path('/files').'/'.$name, $name);
            }             
            $zip->close();
        }
        $headers = array(
            'Content-Type' => 'application/octet-stream',
        );
        $filetopath=public_path('/files').'/'.$zipFileName;
        if(file_exists($filetopath)){
            // return response()->download($filetopath,$zipFileName,$headers)->deleteFileAfterSend(true);
        }
    }

    public function download_all($type){
        $category = "";
        if($type == "leasing_contracts"){
            $category = "Leasing Contracts";
        }else if($type == "license_permits"){
            $category = "License Permits";
        }else if($type == "hr"){
            $category = "Hr";
        }else if($type == "permits"){
            $category = "Permits";
        }else if($type == "operations"){
            $category = "Operations";
        }else if($type == "establishment"){
            $category = "Establishment";
        }        

        if($category != ""){
            $files = Filu::where(['category' => $category])->get();
        }
    }
}
