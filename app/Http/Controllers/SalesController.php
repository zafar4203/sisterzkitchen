<?php

namespace App\Http\Controllers;

use App\Sale;
use App\Product;
use App\Order;
use Carbon\Carbon;
use App\OrderItem;
use DataTables;
use Illuminate\Http\Request;

class SalesController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth:staff');
    }
    
    public function getSales($year , $month){
        if($month < 10){
            $month = sprintf("%02d", $month);
        }
        $sales = Sale::select('id', 'items', 'total' ,'date' , 'delivery' ,  'created_at')
        ->whereYear('created_at', '=', $year)
        ->whereMonth('created_at', '=', $month)
        ->get();

        return DataTables::of($sales)
        ->addColumn('action', function ($id) {
            $date = date('Y-m-d', strtotime($id->date));
            return '<a href="'.url("saleDetails").'/'.$date.'" style="text-decoration:underline; cursor:pointer;" class="text-primary">View</a>'; 
        })
        ->make(true);
    }

    public function getSalesDate($start , $end){

        $sales = Sale::select('id', 'items', 'total' ,'date' , 'delivery' ,  'created_at')
        // ->whereBetween('created_at', [$start, $end])
        ->whereDate('created_at', '>=', $start)
        ->whereDate('created_at', '<=', $end)
        ->get();

        return DataTables::of($sales)
        ->addColumn('action', function ($id) {
            return '<a href="'.url("saleDetails").'/'.$id->created_at->format('Y-m-d').'" style="text-decoration:underline; cursor:pointer;" class="text-primary">View</a>'; 
        })
        ->make(true);
    }

    public function index(){
        return view('sales.sales');
    }

    public function saleDetails($date){
        $products = Product::all();
        foreach($products as $product){
            $product->count = 0;
        }

        $total = 0;
        $items = 0;
        $discount = 0;
        $delivery = 0;
        $orders = Order::where(['order_status' => '2'])
        ->whereDate('created_at' , $date)->get();

        if($orders)
        foreach($orders as $order){
            $orderItems = OrderItem::where(['order_id' => $order->id])->get();
            foreach($orderItems as $orderItem){
                  foreach($products as $product){
                      if($orderItem->product_id == $product->id){
                          $product->count = $product->count + $orderItem->product_quantity; 
                      }
                  }  
            }
            $discount = $discount + $order->discount;
            $delivery = $delivery + $order->delivery;
        }

        foreach($products as $product){
            if($product->count > 0){
                $total = $total + ((float)$product->count * (float)$product->price);                        
                $items++;
            }
        }  

        return view('sales.saleDetails' , compact('products' , 'date' , 'total', 'items','discount','delivery'));
    }

    public function saleItemsMonth(){
        return view('sales.saleItemsMonth');
    }

    public function getSalesItems($year , $month){
        if($month < 10){
            $month = sprintf("%02d", $month);
        }

        $products = Product::all();
        foreach($products as $product){
            $product->count = 0;
            $product->total = 0;
            $product->red = 0;
        }

        $orders = Order::where(['order_status' => '2'])
        ->whereYear('created_at', '=', $year)
        ->whereMonth('created_at', '=', $month)
        ->get();
        
        $total = 0;
        $items = 0;
        foreach($orders as $order){
            $orderItems = OrderItem::where(['order_id' => $order->id])->get();
            foreach($orderItems as $orderItem){
                  foreach($products as $product){
                      if($orderItem->product_id == $product->id){
                          $product->count = $product->count + $orderItem->product_quantity; 
                          $product->total = $product->count * $product->price;
                          $total = $total + $product->count * $product->price;
                          $product->red = 1;
                          $items++;
                      }
                  }  
            }
        }

        return DataTables::of($products)
        ->editColumn('image', function ($id) {
            return '<img class="green image" src="'.$id->image.'" />'; 
        })
        ->rawColumns(['image'])
        ->make(true);
    }
}
