<?php

namespace App\Http\Controllers;

use Validator;
use DataTables;
use App\Expense;
use Illuminate\Http\Request;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        // $this->middleware('auth:staff');
    }
    
    public function getExpenses($year , $month){
        if($month < 10){
            $month = sprintf("%02d", $month);
        }
        $expenses = Expense::select('id', 'amount', 'description' ,'date' , 'name' ,  'created_at')
        ->whereYear('date', '=', $year)
        ->whereMonth('date', '=', $month)
        ->get();

        return DataTables::of($expenses)
        ->addColumn('action', function ($id) {
            return '<a href="'. route("expenses.edit" , $id->id) .'" class="text-danger"><i class="fa fa-edit"></i></a>
            <a class="text-danger" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></button>'; 
        })
        ->make(true);
    }

    public function index()
    {
        return view('expenses.expenses');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('expenses.addExpense');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|max:50', 
            'description' => 'regex:/^[\pL\s\.]+$/u|max:500', 
            'amount' => 'required|digits_between:1,6',
            'date' => 'required'
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $expense = Expense::create([
            'name' => $request->name , 
            'description' => $request->description , 
            'amount' => $request->amount , 
            'date' => $request->date 
             ]);
        $expense->save();

        return redirect()->back()->with('message', 'Expense Added Successfully');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expense = Expense::where(['id' => $id])->first();
        return view('expenses.editExpense' , compact('expense'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|max:50', 
            'description' => 'regex:/^[\pL\s\.]+$/u|max:500', 
            'amount' => 'required|digits_between:1,6',
            'date' => 'required'
            ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator);
        }

        $expense = Expense::where(['id' => $request->id])->first();
        $expense->name = $request->name;
        $expense->amount = $request->amount;
        $expense->description = $request->description;
        $expense->date = $request->date;
        
        $expense->save();

        return redirect('expenses')->with('message', 'Expense Edit Successfully');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        Expense::where(['id' => $id])->delete();
        
        return redirect()->back()->with('message', 'Expense Deleted Successfully');;
    }
}
