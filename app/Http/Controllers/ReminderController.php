<?php

namespace App\Http\Controllers;

use App\Reminder;
use App\User;
use Notification;
Use DataTables;
use Validator;  
use DateTime;  
use Carbon;
use Illuminate\Http\Request;
use App\Notifications\AdminNotification;

class ReminderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getReminders(){
        $reminders = Reminder::all();

        foreach($reminders as $reminder){
            $reminder->status = Reminder::block($reminder->status);
        }

        return DataTables::of($reminders)
        ->addColumn('action', function ($id) {
            return '<a href="'. route("reminders.edit" , $id->id) .'" class="text-danger"><i class="fa fa-edit"></i></a>
            <a class="text-danger" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></button>'; 
        })
        ->make(true);
    }

    public function index()
    {
        return view('reminders.reminders');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('reminders.addReminder');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'title' => 'required|max:200', 
            'description' => 'regex:/^[\pL\s\.]+$/u|max:500', 
            'amount' => 'required|numeric',
            'status' => 'required',
            'submit_date' => 'required'
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $reminder = Reminder::create([
            'title' => $request->title , 
            'description' => $request->description , 
            'amount' => $request->amount , 
            'status' => $request->status , 
            'submit_date' => $request->submit_date 
             ]);
        $reminder->save();

        return redirect('reminders')->with('message', 'Reminder Added Successfully');;
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reminder = Reminder::where(['id' => $id])->first();
        return view('reminders.editReminder' , compact('reminder'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'title' => 'required|max:200', 
            'description' => 'regex:/^[\pL\s\.]+$/u|max:500', 
            'amount' => 'required|numeric',
            'status' => 'required',
            'submit_date' => 'required'
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator);
        }

        $reminder = Reminder::where(['id' => $request->id])->first();
        $reminder->title = $request->title;
        $reminder->amount = $request->amount;
        $reminder->description = $request->description;
        $reminder->submit_date = $request->submit_date;
        $reminder->status = $request->status;
        
        $reminder->save();

        return redirect('reminders')->with('message', 'Reminder Updated Successfully');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        Reminder::where(['id' => $id])->delete();   
        return redirect()->back()->with('message', 'Reminder Deleted Successfully');;
    }

    public function checkReminder(){
        $today = date('m/d/Y');

        $reminders = Reminder::where(['status' => 0])->get();
        foreach($reminders as $reminder){
            $submit_date = $reminder->submit_date;
            $days = Carbon\Carbon::parse(now())->diffInDays($reminder->submit_date, false);
            if($days<=5){
                $this->sendEmail($days , $reminder);
            }
            if($days <= 0){
                $rem = Reminder::where(['id' => $reminder->id])->first();
                $rem->status = 1;
                $rem->save();
            }
        }

        return response()->json(array('success' => true), 200);
    }

    public function sendEmail($days , $reminder){
        $date = date('m/d/Y');
        $splitDate = explode('/', $date);

        $month = $splitDate[0];
        $day = $splitDate[1];
        $year = $splitDate[2];

        $date = $day . "-".date('F', mktime(0, 0, 0, $month, 10))."-".$year; // March

        $last_name = !empty($splitName[1]) ? $splitName[1] : ''; // If last name doesn't exist, make it empty

        $details = [
            'greeting' => 'Hi Admin',
            'subject' => 'Sisterz Kitchen | Reminder | Reminder Id #'.$reminder->id,
            'body' => '',
            'thanks' => 'Thank you for choosing Sisterz Kitchen !',
            'order_id' => $reminder->id
        ];

        $details['body'] = $details['body'] . "<b>" . $days . " days remaining for ". $reminder->title."</b><br><br>"; 
        $details['body'] = $details['body'] . "Title : " . $reminder->title . "<br>";  
        $details['body'] = $details['body'] . "Amount : Rs " . $reminder->amount . "<br>";  
        $details['body'] = $details['body'] . "...........................................................<br>";  
        $details['body'] = $details['body'] . $reminder->description ."<br>";         
        $details['body'] = $details['body'] . "............................................................<br>";  
        $details['body'] = $details['body'] . "For further information. Kindly visit the Portal <br><br>";  
        $details['body'] = $details['body'] . "Sisterzkitchen";  

        $user = new User();
        $user->email="sisterzkitchen1@gmail.com";
        Notification::send($user , new AdminNotification($details));
    }
}
