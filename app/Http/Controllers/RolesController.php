<?php

namespace App\Http\Controllers;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Validator;
use DataTables;
use Illuminate\Http\Request;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        // $this->middleware('auth:staff');
    }

    public function getRoles(){
        $roles = Role::select('id', 'name' , 'created_at')->get();

        return DataTables::of($roles)
        ->editColumn('name' , function($id){
            return '<a class="text-danger" href="'.url("permissions").'/'.$id->id.'" >'.$id->name.'</a>';
        })
        ->addColumn('action', function ($id) {
            return '<a href="'.url("editRole").'/'.$id->id.'" class="text-danger"><i class="fa fa-edit"></i></a>
            <a class="text-danger" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></button>'; 
        })
        ->rawColumns(['name' , 'action'])
        ->make(true);
    }

    public function index()
    {
        return view("roles.roles");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("roles.addRole");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255', 
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $role = Role::create(['guard_name' => 'staff', 'name' => $request->name]);
        $role->save();

        return redirect('roles')->with('message', 'Role Added Successfully');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::where(['id' => $id])->first();
        return view('roles.editRole' , compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255'
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $role = Role::where(['id' => $request->id])->first();

        $role->name = $request->name;
        $role->save();

        return redirect('roles')->with('message', 'Role Updated Successfully');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $role = Role::where(['id' => $id])->first();        
        $permissions = $role->permissions;
        foreach($permissions as $permission){
            $role->revokePermissionTo($permission);
        }
        $role->delete();
        return redirect()->back()->with('message', 'Role Deleted Successfully');;
    }

    public function permission($id){
        $role = Role::where(['id' => $id])->first();
        $permissions = $role->permissions;

        $sales = false;
        $products = false;
        $testimonials = false;
        $orders = false;
        $financials = false;
        $staff = false;
        $files = false;
        $home = false;
        $reminders = false;

        foreach($permissions as $permission){
            if($permission->name == 'sales'){
                $sales = true;
            }
            if($permission->name == 'products'){
                $products = true;
            }
            if($permission->name == 'testimonials'){
                $testimonials = true;
            }
            if($permission->name == 'orders'){
                $orders = true;
            }
            if($permission->name == 'financials'){
                $financials = true;
            }
            if($permission->name == 'staff'){
                $staff = true;
            }
            if($permission->name == 'files'){
                $files = true;
            }
            if($permission->name == 'home'){
                $home = true;
            }
            if($permission->name == 'reminders'){
                $reminders = true;
            }
        }

        return view('permissions.permissions' , compact('id' , 'sales' , 'products' , 'testimonials' , 'orders' , 'financials' , 'staff' , 'files' , 'home' , 'reminders'));
    }

    public function addPermission($id , $name){
        $role = Role::where(['id' => $id])->first();
        $permission = Permission::where(['name' => $name])->first();
        if(!$permission){
            $permission = Permission::create(['guard_name' => 'staff','name' => $name]);
        }
        $role->givePermissionTo($permission);
        return response()->json('success', 200);        
    }

    public function removePermission($id , $name){
        $role = Role::where(['id' => $id])->first();
        $permission = Permission::where(['name' => $name])->first();
        $role->revokePermissionTo($permission);
        
        return response()->json('success', 200);        
    }
}
