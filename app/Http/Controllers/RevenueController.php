<?php

namespace App\Http\Controllers;

use Validator;
use DataTables;
use App\Revenue;
use Illuminate\Http\Request;

class RevenueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        // $this->middleware('auth:staff');
    }
    
    public function getRevenues($year , $month){
        if($month < 10){
            $month = sprintf("%02d", $month);
        }
        $expenses = Revenue::select('id', 'amount', 'description' ,'date' , 'name' ,  'created_at')
        ->whereYear('date', '=', $year)
        ->whereMonth('date', '=', $month)
        ->get();

        return DataTables::of($expenses)
        ->addColumn('action', function ($id) {
            return '<a href="'. route("revenues.edit" , $id->id) .'" class="text-danger"><i class="fa fa-edit"></i></a>
            <a class="text-danger" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></button>'; 
        })
        ->make(true);
    }

    public function index()
    {
        return view('revenues.revenues');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('revenues.addRevenue');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|max:50', 
            'description' => 'regex:/^[\pL\s\.]+$/u|max:500', 
            'amount' => 'required|digits_between:1,6',
            'date' => 'required'
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $revenue = Revenue::create([
            'name' => $request->name , 
            'description' => $request->description , 
            'amount' => $request->amount , 
            'date' => $request->date 
             ]);
        $revenue->save();

        return redirect()->back()->with('message', 'Revenue Added Successfully');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $revenue = Revenue::where(['id' => $id])->first();
        return view('revenues.editRevenue' , compact('revenue'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|max:50', 
            'description' => 'regex:/^[\pL\s\.]+$/u|max:500', 
            'amount' => 'required|digits_between:1,6',
            'date' => 'required'
            ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator);
        }

        $revenue = Revenue::where(['id' => $request->id])->first();
        $revenue->name = $request->name;
        $revenue->amount = $request->amount;
        $revenue->description = $request->description;
        $revenue->date = $request->date;
        
        $revenue->save();

        return redirect('revenues')->with('message', 'Revenue Edit Successfully');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        Revenue::where(['id' => $id])->delete();
        return redirect()->back()->with('message', 'Revenue Deleted Successfully');;
    }
}
