<?php

namespace App\Http\Controllers;

use Validator;
use DataTables;
use App\Testimonial;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        // $this->middleware('auth:staff');
    }
    
    public function getTestimonials(){
        $testimonials = Testimonial::all();

        return DataTables::of($testimonials)
        ->addColumn('action', function ($id) {
            return '<a href="'. route("testimonials.edit" , $id->id) .'" class="text-danger"><i class="fa fa-edit"></i></a>
            <a class="text-danger" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></button>'; 
        })
        ->make(true);
    }

    public function index()
    {
        return view('testimonials.testimonials');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('testimonials.addTestimonial');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'title' => 'required|max:200', 
            'description' => 'regex:/^[\pL\s\.]+$/u|max:500', 
            'author' => 'required|max:100',
            'date' => 'required'
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $testimonial = Testimonial::create([
            'title' => $request->title , 
            'description' => $request->description , 
            'author' => $request->author , 
            'date' => $request->date 
             ]);
        $testimonial->save();

        return redirect('testimonials')->with('message', 'Testimonial Added Successfully');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $testimonial = Testimonial::where(['id' => $id])->first();
        return view('testimonials.editTestimonial' , compact('testimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'title' => 'required|max:200', 
            'description' => 'regex:/^[\pL\s\.]+$/u|max:500', 
            'author' => 'required|max:100',
            'date' => 'required'
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator);
        }

        $testimonial = Testimonial::where(['id' => $request->id])->first();
        $testimonial->title = $request->title;
        $testimonial->author = $request->author;
        $testimonial->description = $request->description;
        $testimonial->date = $request->date;
        
        $testimonial->save();

        return redirect('testimonials')->with('message', 'Testimonial Edit Successfully');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        Testimonial::where(['id' => $id])->delete();   
        return redirect()->back()->with('message', 'Testimonial Deleted Successfully');;
    }
}
