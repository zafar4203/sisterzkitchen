<?php

namespace App\Http\Controllers;

use App\Expense;
use App\Revenue;
use App\Sale;
use Carbon\Carbon;
use App\Financial;
use DataTables;
use Illuminate\Http\Request;

class FinancialController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:staff');
    }
    
    public function financials(){
        return view('financials.financials');
    }

    public function getFinancials($year , $month){
        if($month < 10){
            $month = sprintf("%02d", $month);
        }
        $expenses = Expense::whereYear('date', '=', $year)
        ->whereMonth('date', '=', $month)
        ->get();

        $revenues = Revenue::whereYear('date', '=', $year)
        ->whereMonth('date', '=', $month)
        ->get();

        $sales = Sale::whereYear('created_at', '=', $year)
        ->whereMonth('created_at', '=', $month)
        ->get();

        $financials = [];
        $total = 0;
        $date = 0;
        $balance = 0;
        if(!$sales->isEmpty()){
            foreach($sales as $sale){
                $total = $total + $sale->total; 
                $date = $sale->date; 
            }

            $balance = $balance + $total;

            $financial = new Financial();
            $financial->title = "Sales";
            $financial->date = $date;
            $financial->debit = "";
            $financial->green = true;
            $financial->credit = $total;
            $financial->balance = $balance;
            
            array_push($financials, $financial);
        }

        foreach($revenues as $revenue){

            $balance = $balance + $revenue->amount;

            $financial = new Financial();
            $financial->date = Carbon::parse($revenue->date)->format('m/d/Y');
            $financial->title = $revenue->name;
            $financial->debit = "";
            $financial->green = true;
            $financial->credit = $revenue->amount;
            $financial->balance = $balance;
            
            array_push($financials, $financial);

        }

        foreach($expenses as $expense){

            $balance = $balance - $expense->amount;

            $financial = new Financial();
            $financial->date = Carbon::parse($expense->date)->format('m/d/Y');
            $financial->title = $expense->name;
            $financial->debit = $expense->amount;
            $financial->green = false;
            $financial->credit = "";
            $financial->balance = $balance;
            
            array_push($financials, $financial);

        }

        return DataTables::of($financials)
        ->make(true);
    }



    public function getFinancialDates($start , $end){
        $expenses = Expense::whereDate('date', '>=', $start)
        ->whereDate('date', '<=', $end)
        ->get();

        $revenues = Revenue::whereDate('date', '>=', $start)
        ->whereDate('date', '<=', $end)
        ->get();

        $sales = Sale::whereDate('created_at', '>=', $start)
        ->whereDate('created_at', '<=', $end)
        ->get();

        $financials = [];
        $total = 0;
        $balance = 0;
        $date = 0;

        if(!$sales->isEmpty()){
            foreach($sales as $sale){
                $total = $total + $sale->total; 
                $date = $sale->date; 
            }

            $balance = $balance + $total;

            $financial = new Financial();
            $financial->title = "Sales";
            $financial->date = $date;
            $financial->debit = "";
            $financial->green = true;
            $financial->credit = $total;
            $financial->balance = $balance;
            
            array_push($financials, $financial);
        }

        foreach($revenues as $revenue){

            $balance = $balance + $revenue->amount;

            $financial = new Financial();
            $financial->date = Carbon::parse($revenue->date)->format('m/d/Y');
            $financial->title = $revenue->name;
            $financial->debit = "";
            $financial->green = true;
            $financial->credit = $revenue->amount;
            $financial->balance = $balance;
            
            array_push($financials, $financial);

        }

        foreach($expenses as $expense){

            $balance = $balance - $expense->amount;

            $financial = new Financial();
            $financial->date = Carbon::parse($expense->date)->format('m/d/Y');
            $financial->title = $expense->name;
            $financial->debit = $expense->amount;
            $financial->green = false;
            $financial->credit = "";
            $financial->balance = $balance;
            
            array_push($financials, $financial);

        }

        return DataTables::of($financials)
        ->make(true);
    }
}
