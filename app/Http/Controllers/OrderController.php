<?php

namespace App\Http\Controllers;

use DataTables;
use App\Order;
use App\Sale;
use App\OrderItem;
use App\OrderCancel;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:staff');
    }

    public function getOrders($year , $month){
        if($month < 10){
            $month = sprintf("%02d", $month);
        }
        $orders = Order::select('id', 'total_price' , 'order_status' , 'created_at')
        ->whereYear('created_at', '=', $year)
        ->whereMonth('created_at', '=', $month)
        ->get();
        
        foreach($orders as $order){
            $order->placed = $order->created_at->format('j F Y');
            $order->total_price = "Rs ".$order->total_price;
            // $order->order_status = Order::orderStatus($order->order_status);
        }

        return DataTables::of($orders)
        ->addColumn('action', function ($id) {
            return '<a href="'.url("orderDetails").'/'.$id->id.'" style="text-decoration:underline; cursor:pointer;" class="text-primary">View</a>'; 
        })
        ->editColumn('order_status', function ($id) {
            return '<span class="'.Order::statusClass($id->order_status).'">'.Order::orderStatus($id->order_status) .'</span>'; 
        })
        ->rawColumns(['order_status', 'action'])
        ->make(true);
    }

    public function index(){
        return view('orders.orders');
    }

    public function create(){
        return view('orders.addOrder');
    }

    public function store(Request $request){}

    public function edit($id){}

    public function update(Request $request){}

    public function delete($id){}

    public function orderDetails($id){
        $order = Order::where(['id' => $id])->first();
        $order->sub_total_price = $order->total_price;
        $number = $order->total_price;
        $number = str_replace(',', '', $number);
        
        $order->delivery_fee = $order->delivery;

        $orders = OrderItem::where('order_id' , '=' , $id)->get();
        return view('orders.orderDetails' , compact('order' , 'orders'));
    }

    public function changeOrderStatus($id , $status){
        $order = Order::where(['id' => $id])->first();
        $order->order_status = $status;
        $order->save();

        if($order->order_status == "2"){
            $this->recordSale($order);
        }
        if($order->order_status != "3"){
            OrderCancel::where(['order_id' => $order->id])->delete();
        }
        return $order;
    }

    public function orderChange(Request $request){
        $order = Order::where(['id' => $request->id])->first();
        $orderItem = OrderItem::where(['id' => $request->item_id])->first();
        $orderItem->product_quantity = $request->quantity;
        $orderItem->save();

        $orderItems = OrderItem::where(['order_id' => $order->id])->get();
        $total = 0;
        foreach($orderItems as $orderItem){
            $total = $total + ((double)$orderItem->product_price * (double) $orderItem->product_quantity);
        }

        if($total < 30){
            $total = (double)$total + 10;
        }

        $order->total_price = number_format($total,2);
        $order->save();

        return $order;
    }

    public function deleteOrderItem($id , $item){
        $order = Order::where(['id' => $id])->first();
        $orderItem = OrderItem::where(['id' => $item])->first();
        $orderItem->delete();

        $orders = OrderItem::where(['order_id' => $order->id])->get();
        $total = 0;
        $count = 0;
        foreach($orders as $orderItem){
            $total = $total + ((double)$orderItem->product_price * (double) $orderItem->product_quantity);
            $count++;
        }

        $order->total_price = number_format($total,2);
        $order->total_items = $count;
        $order->save();

        return redirect()->back();
    }

    public function updateOrder($id){
        $order = Order::where(['id' => $id])->first();
        $orders = OrderItem::where(['order_id' => $order->id])->get();
        $total = 0;
        $count = 0;
        foreach($orders as $orderItem){
            $total = $total + ((double)$orderItem->product_price * (double) $orderItem->product_quantity);
            $count++;
        }

        $order->total_price = number_format($total,2);
        $order->total_items = $count;
        $order->save();

        return response()->json('success' , 200);
    }

    public function orderUserAddress(Request $request){
        $order = Order::where(['id' => $request->order_id])->first();
        $order->user_address = $request->user_address;
        $order->save();

        return $order;
    }

    public function recordSale($order){
        $date = $order->created_at->format('m/d/Y');
        $sale = Sale::where(['date' => $date])->first();

        $order->total_price = (float) str_replace(',', '', $order->total_price);
        $order->total_items = (float) str_replace(',', '', $order->total_items);
        if($sale){
            $sale->total = (float) $sale->total + (float) $order->total_price;
            $sale->items = (float) $sale->items + (float) $order->total_items; 
            $sale->delivery = (float)$sale->delivery + (float) $order->delivery; 
            $sale->date = $date;
            $sale->save();
        }else{
            Sale::create([
                'total' => (float) $order->total_price ,
                'items' => (float) $order->total_items ,
                'date' => $date,
                'delivery' => (float) $order->delivery
            ]);
        }
    }
}
