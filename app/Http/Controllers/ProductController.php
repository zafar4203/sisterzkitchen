<?php

namespace App\Http\Controllers;

use App\Product;
use App\Image;
use File;
use DataTables;
use Validator;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        // $this->middleware('auth:staff');
    }
        
     public function index(){
         return view('products.products');
     }
    
     public function getAjaxProducts()
    {
        $products = Product::select('id', 'name', 'short_name', 'block', 'image', 'price', 'unit' , 'created_at')
        ->get();

        foreach($products as $product){
            $product->block = Product::block($product->block);
        }

        return DataTables::of($products)
        ->addColumn('action', function ($id) {
            return '<a href="'. route("products.edit" , $id->id) .'" class="text-danger"><i class="fa fa-edit"></i></a>
            <a class="text-danger" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></button>'; 
        })
        ->editColumn('image', function ($product) { 
            return '<img src='.$product->image.' border="0" width="40" class="img-rounded" align="center" />'; 
         })
         ->rawColumns(['image', 'action'])
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.addProduct');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|max:50', 
            'short_name' => 'regex:/^[\pL\s\.]+$/u|max:20', 
            'price' => 'required|digits_between:1,6',
            'image' => 'required',
            'unit' => 'required'
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $product = Product::create([
            'name' => $request->name , 
            'short_name' => $request->short_name , 
            'price' => $request->price , 
            'block' => $request->block , 
            'unit' => $request->unit ,
            'image' => '' 
        ]);
        $product->save();

        $imageUrl = "";

        if($request->hasfile('image')){
            $files = $request->file('image');
            for($i=0 ; $i<count($files) ; $i++){
                $img = "";
                $file = $files[$i];
                $filename = time().$i.'.'.$file->getClientOriginalExtension();
                $destinationPath = public_path('/images');
                $file->move($destinationPath, $filename);    
                $img = asset("public/images/".$filename);
                $image = new Image();
                $image->name = $filename;
                $image->imageUrl = $img;
                $image->product_id = $product->id;
                $image->save();
    
                if($i == 0){
                    $imageUrl = asset("public/images/".$filename);
                }
            }
        }

        $productUpdate = Product::where(['id' => $product->id])->first();
        if($request->mob){
            $productUpdate->mob = 1;
        }else{
            $productUpdate->mob = 0;
        }
        if($request->web){
            $productUpdate->web = 1;
        }else{
            $productUpdate->web = 0;
        }
        if($request->adm){
            $productUpdate->adm = 1;
        }else{
            $productUpdate->adm = 0;            
        }        
        $productUpdate->image = $imageUrl;
        $productUpdate->save();

        return redirect('products')->with('message', 'Product Added Successfully');;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::where(['id' => $id])->first();
        return view('products.editProduct' , compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|max:50', 
            'short_name' => 'regex:/^[\pL\s\.]+$/u|max:20', 
            'price' => 'required|digits_between:1,8',
            'unit' => 'required'
            ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator);
        }

        $product = Product::where(['id' => $request->id])->first();

        $product->name = $request->name;
        $product->short_name = $request->short_name;
        $product->price = $request->price;
        $product->block = $request->block;
        $product->unit = $request->unit;
        if($request->mob){
            $product->mob = 1;
        }else{
            $product->mob = 0;
        }
        if($request->web){
            $product->web = 1;
        }else{
            $product->web = 0;
        }
        if($request->adm){
            $product->adm = 1;
        }else{
            $product->adm = 0;            
        }
        $imageUrl = "";

        if($request->hasfile('image')){
            // Removing previous Images
            $images = Image::where(['product_id' => $request->id])->get();
            foreach($images as $image){
                $destinationPath = public_path('/images')."/".$image->name;
                if(File::exists($destinationPath)) {
                    File::delete($destinationPath);            
                }
                $image->delete();
            }            
            
            $files = $request->file('image');
            for($i=0 ; $i<count($files) ; $i++){
                $img = "";
                $file = $files[$i];
                $filename = time().$i.'.'.$file->getClientOriginalExtension();
                $destinationPath = public_path('/images');
                $file->move($destinationPath, $filename);    
                $img = asset("public/images/".$filename);
                $image = new Image();
                $image->name = $filename;
                $image->imageUrl = $img;
                $image->product_id = $product->id;
                $image->save();
    
                if($i == 0){
                    $imageUrl = asset("public/images/".$filename);
                }
            }

            $product->image = $imageUrl;
        }

        $product->save();

        return redirect()->back()->with('message', 'Product Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        
    }

    public function deleteProduct($id){
        $images = Image::where(['product_id' => $id])->get();
        foreach($images as $image){
            $destinationPath = public_path('/images')."/".$image->name;
            if(File::exists($destinationPath)) {
                File::delete($destinationPath);            
            }
            $image->delete();
        }

        Product::where(['id' => $id])->delete();
        
        return redirect()->back()->with('message', 'Product Deleted Successfully');;
    }
}
