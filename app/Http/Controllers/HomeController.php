<?php

namespace App\Http\Controllers;

use Auth;
use App\Sale;
use App\Expense;
use App\Order;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth:staff');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        $year = date('Y');
        $month = date('m');
        $yearSale = 0;
        $monthSale = 0;
        $monthSaleChart = 0;
        $daySale = 0;

        // Year Sale
        $sales = Sale::whereYear('created_at', '=', $year)->get();
        if($sales){
            foreach($sales as $sale){
                $yearSale = $yearSale + $sale->total;
            }
        }

        // Monthly Sale
        $sales = Sale::whereYear('created_at', '=', $year)
        ->whereMonth('created_at', '=', $month)
        ->get();
        if($sales){
            foreach($sales as $sale){
                $monthSale = $monthSale + $sale->total;
            }
        }
        $monthSaleChart = $monthSale;

        // Today Sale
        $date = date('m/d/Y');
        $sale = Sale::where(['date' => $date])->first();
        if($sale){
            $daySale = $sale->total;
        }

        // Pending Orders
        $pOrders = 0;
        $orders = Order::where(function ($query) {
            $query->where('order_status', '=', '0')
                  ->orWhere('order_status', '=', '1');
        })->get();
        foreach($orders as $order){
            $pOrders++;
        }

        // Sales And Expenses Chart Monthly
        $expenses = Expense::whereYear('date', '=', $year)
        ->whereMonth('date', '=', $month)
        ->get();
        $expense_amount = 0;
        foreach($expenses as $expense){
            $expense_amount = $expense_amount + $expense->amount;
        }

        $exp_percent = 0;
        $sale_percent = 0;
        if($monthSaleChart != 0 ){
            $exp_percent = (int) (($expense_amount/$monthSaleChart) * 100);
        }
        $sale_percent = 100 - $exp_percent;    
        $Data = array($sale_percent , $exp_percent);


        // Area Chart Data
        $months = array('Jan' , 'Feb' , 'Mar' , 'Apr' , 'May' , 'Jun' , 'Jul' , 'Aug' , 'Sep' , 'Oct' , 'Nov' , 'Dec');
        // Monthly Sale
        $months_sales = array($this->monthSales(1) , $this->monthSales(2) , $this->monthSales(3) , $this->monthSales(4), $this->monthSales(5), $this->monthSales(6), $this->monthSales(7), $this->monthSales(8), $this->monthSales(9), $this->monthSales(10), $this->monthSales(11), $this->monthSales(12));
        
        if(Auth::guard('staff')->check()){
            if(Auth::guard('staff')->user()->can('home')){
                return view('dashboard' ,['Data' => $Data , 'months' => $months , 'months_sales' => $months_sales]  , compact('yearSale' , 'monthSale' , 'daySale' , 'pOrders' , 'sale_percent'));
            }else{
                $permissions = Auth::guard('staff')->user()->getAllPermissions();
                return redirect($permissions[0]->name);
            }
        }else{
            return view('dashboard' ,['Data' => $Data , 'months' => $months , 'months_sales' => $months_sales]  , compact('yearSale' , 'monthSale' , 'daySale' , 'pOrders' , 'sale_percent'));
        }
    }

    public function monthSales($mon){
        if($mon < 10){
            $mon = sprintf("%02d", $mon);
        }
        $year = date('Y');
        $total = 0;
        $sales = Sale::whereYear('created_at', '=', $year)
        ->whereMonth('created_at', '=', $mon)
        ->get();
        if($sales){
            foreach($sales as $sale){
                $total = $total + $sale->total;
            }
        }

        return $total;
    }
}
