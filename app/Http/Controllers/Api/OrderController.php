<?php

namespace App\Http\Controllers\Api;

use PDF;
use Validator;
use Notification;
use App\Order;
use Carbon\Carbon;
use App\User;
use App\Jobs\EmailJob;
use App\OrderItem;
use App\Client;
use App\OrderCancel;
use Illuminate\Http\Request;
use App\Notifications\UserNotification;
use App\Notifications\AdminNotification;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    public function placeOrderWeb(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_name' => 'required|regex:/^[\pL\s\-]+$/u|max:50', 
            'user_phone' => 'required|digits:11', 
            'user_address' => 'required|max:150',
            'order_from' => 'required',
            'total_price' => 'required',
            'total_items' => 'required',
        ]);

        if ($validator->fails()) { 
            return response()->json($validator->messages(), 403);
        }

        $total_price = $request->total_price;
        if($request->recieved_amount != 0){
            $total_price = $request->recieved_amount;
        }
      
        $order = Order::create([
            'user_name' => $request->user_name,
            'user_phone' => $request->user_phone,
            'user_email' => $request->user_email,
            'user_address' => $request->user_address,
            'order_from' => "WEB" , 
            'total_price' => $total_price , 
            'total_items' => $request->total_items , 
            'order_status' => "0",
            'discount' => $request->discount,
            'delivery' => $request->delivery_fee,
            'created_at' => $request->date    
        ]);


        // $files = public_path('assets/temp_files/*'); //get all file names
        // foreach($files as $file){
        //     if(is_file($file))
        //     unlink($file); //delete file
        // }

        return response()->json(array('success' => true , 'order' =>$order), 200);        
    }

    public function orderItems(Request $request){
        OrderItem::create([
            'order_id' => $request->order_id,
            'product_id' => $request->product_id,
            'product_name' => $request->product_name,
            'product_price' => $request->product_price,
            'product_quantity' => $request->product_quantity,
        ]);

        return response()->json('success' , 200);
    }

    public function placeOrderAndroid(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_name' => 'required|regex:/^[\pL\s\-]+$/u|max:50', 
            'user_phone' => 'required|digits:11',
            'user_email' => 'required|email',
            'user_address' => 'required|max:150',
            'total_price' => 'required',
            'total_items' => 'required'
        ]);

        if ($validator->fails()) { 
            return response()->json($validator->messages(), 403);
        }


        $client = Client::where(['phone' => $request->user_phone])->first();
        if(!$client){              
            return response()->json(array('not_verified' => true) , 403);
        }

        $total_price = $request->total_price;
        $number = $total_price;
        $number = str_replace(',', '', $number);
        if($number < 1199){
            $request->delivery_fee = 120;
        }else{
            $request->delivery_fee = 0;
        }

        $order = Order::create([
            'user_name' => $request->user_name,
            'user_phone' => $request->user_phone,
            'user_email' => $request->user_email,
            'user_address' => $request->user_address,
            'order_from' => "POS",
            'total_price' => $request->total_price,
            'total_items' => $request->total_items,
            'order_status' => "0",  
            'order_note' =>  $request->order_note,
            'delivery' =>  $request->delivery_fee
        ]);

        foreach($request->order_items as $item){
            OrderItem::create([
                'order_id' => $order->id,
                'product_id' => $item['product_id'],
                'product_name' => $item['product_name'],
                'product_price' => $item['product_price'],
                'product_quantity' => $item['quantity'],
            ]);
        }

        $job  = (new EmailJob($order->id))->delay(now());
        dispatch($job);

        return response()->json($order, 200);
    }
    public function orderCancel(request $request){
        if($request->cancel_comments == null){
            $request->cancel_comments = "Order canceled By User";
        }

        $orderCancel = OrderCancel::updateOrCreate(
            ['order_id' => $request->order_id],
            ['user_id' => $request->user_id , 'cancel_comments' => $request->cancel_comments]
        );

        return response()->json(array('success' => true), 200);
    }

    public function sendOrderEmails(Request $request){

        $date = date('m/d/Y');
        $splitDate = explode('/', $date);

        $month = $splitDate[0];
        $day = $splitDate[1];
        $year = $splitDate[2];

        $date = $day . "-".date('F', mktime(0, 0, 0, $month, 10))."-".$year; // March

        $last_name = !empty($splitName[1]) ? $splitName[1] : ''; // If last name doesn't exist, make it empty

        $order = Order::where(['id' => $request->order_id])->first();
        
        $orderItems = OrderItem::where(['order_id' => $request->order_id])->get();

        $order->sub_total_price = $order->total_price;
        $order->total_price = (int)$order->sub_total_price + (int) $order->delivery;

        $details = [
            'greeting' => 'Hi '.$request->user_name,
            'subject' => 'Sisterz Kitchen | Order Received | Order Id #'.$order->id,
            'body' => '',
            'thanks' => 'Thank you for choosing Sisterz Kitchen !',
            'order_id' => $request->order_id
        ];

        $details['body'] = $details['body'] . "<b> Order Id : " . $order->id . "</b><br><br>"; 
        foreach($orderItems as $orderItem){
            $details['body'] = $details['body'] . $orderItem->product_name . " x " . $orderItem->product_quantity . "<br>"; 
        }
        $details['body'] = $details['body'] . "...........................................................<br>";  
        $details['body'] = $details['body'] . "Sub Total : Rs " . $order->sub_total_price . "<br>";  
        $details['body'] = $details['body'] . "Delivery : Rs " . $order->delivery . "<br>";  
        $details['body'] = $details['body'] . "Total : Rs " . $order->total_price . "<br>";  
        $details['body'] = $details['body'] . "............................................................<br>";  
        $details['body'] = $details['body'] . "Placed At : " .$date ." <br>";  
        $emailBody = $details['body'];
        $details['body'] = $details['body'] . "For assistance, feel free to call us at this number +92 333 8071764 <br>";  

        $order->email = $order->user_email;
        // Notification::send($order , new UserNotification($details));
        try{
            $data = [
                'details' => $details['body']
            ];
        Mail::send('mail.mailBody',$data, function ($message) use ($data,$order) {
            $message->from('sisterzkitchen1@gmail.com','SisterzKitchen');
            $message->to($order->user_email);
            $message->subject('Sisterz Kitchen | Order Received | Order Id #'.$order->id);
            $fileName = public_path('assets/temp_files/').str_random(4).time().'.pdf';
            $pdf = PDF::loadView('load.order_print', compact('order'))->save($fileName);    
            $message->attach($fileName);
        });
        }
        catch (\Exception $e){
            dd($e);
            // Never reached
        }
 
        $files = glob('assets/temp_files/*'); //get all file names
        foreach($files as $file){
            if(is_file($file))
            unlink($file); //delete file
        }

        $details = [
            'subject' => 'New order recieved',
            'body' => '',
        ];
        $details['body'] = $emailBody;        
        $details['body'] = $details['body'] . "............................................................<br>";  
        $details['body'] = $details['body'] . "Order Time : " . $order->created_at . "<br>";        
        $details['body'] = $details['body'] . "Customer Name : " . $order->user_name . "<br>";        
        $details['body'] = $details['body'] . "Customer Phone : " . $order->user_phone . "<br>";        
        $details['body'] = $details['body'] . "Customer Address : " . $order->user_address . "<br>";        

        $user = new User();
        $user->email="sisterzkitchen1@gmail.com";
        Notification::send($user , new AdminNotification($details));

        return response()->json('success', 200);        
    }

    public function getOrders($phone){
        $orders = Order::where(['user_phone' => $phone])
        ->orderBy('id' , 'desc')
        ->get();
        foreach($orders as $order){
            foreach($order->order_items as $item){
                $item->quantity = $item->product_quantity;
            }
        }
        return response()->json($orders, 200);
    }
}
