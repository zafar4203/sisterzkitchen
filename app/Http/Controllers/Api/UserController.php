<?php

namespace App\Http\Controllers\Api;

use Validator;
use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function register(Request $request) 
    { 
        $input = $request->all();
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|unique:users|email', 
            'password' => 'required'
        ]);
        if ($validator->fails()) { 
            return response()->json($validator->messages(), 403);
        }
         
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $user = User::where('email', '=', $input['email'])->first();
        return response()->json($user, 200); 
    }

    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 

            return response()->json($user, 200); 
        } 
        else{ 
            return response()->json(['email'=> ["Incorrect login credentials"]], 401); 
        } 
    }
}
