<?php

namespace App\Http\Controllers\Api;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function products(){
        $products = Product::where(['block'  => 0])->get();
        return response()->json($products, 200);
    }

    public function productsWeb(){
        $products = Product::where(['block'  => 0])->where(['web'  => 1])->get();
        return response()->json($products, 200);
    }

    public function productsMob(){
        $products = Product::where(['block'  => 0])->where(['mob'  => 1])->get();
        return response()->json($products, 200);
    }

    public function productsAdm(){
        $products = Product::where(['block'  => 0])->where(['adm'  => 1])->get();
        return response()->json($products, 200);
    }
}