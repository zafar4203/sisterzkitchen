<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'short_name' , 'price', 'block', 'image', 'unit', 'web', 'mob', 'adm'];
    public static function block($val){
        if($val == 0)
        return "Approve";
        else
        return "Block";
    }
}
