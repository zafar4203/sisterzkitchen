<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;

class Staff extends Authenticatable
{
    use HasRoles;
    protected $guard_name = 'staff';

    protected $fillable = [
        'name',
        'email',
        'password',
        'mobile',
        'gender',
        'age',
        'cnic',
        'address',
        'block',
        'role',
        'remember_token'
    ];

    public static function block($val){
        if($val == 0)
        return "Approve";
        else
        return "Block";
    }

    public static function getPermission($id , $name){
        $role = Role::where(['id' => $id])->first();
        $permissions = $role->permissions;
        foreach($permissions as $permission){
            if($permission->name == $name){
                return true;
            }
        }
        return false;
    }
}
