<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use Notifiable;
    protected $fillable = [
        'user_name' ,
        'user_email' ,
        'user_phone' ,
        'user_address' , 
        'user_id' , 
        'order_from' , 
        'total_price' , 
        'total_items' ,  
        'delivery' ,  
        'user_id', 
        'order_status', 
        'order_note', 
        'created_at',
        'discount'];

        public static function orderStatus($val){
            if($val == "0"){
                return "New Order";
            }
            if($val == "1"){
                return "On Way";
            }
            if($val == "2"){
                return "Delivered";
            }
            if($val == "3"){
                return "Canceled";
            }
        }

        public static function statusClass($val){
            if($val == "0"){
                return "new";
            }
            if($val == "1"){
                return "on-way";
            }
            if($val == "2"){
                return "delivered";
            }
            if($val == "3"){
                return "canceled";
            }
        }

        public function order_items()
        {
            return $this->hasMany('App\OrderItem');
        }
        
}
