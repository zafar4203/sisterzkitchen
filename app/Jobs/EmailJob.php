<?php

namespace App\Jobs;

use App\User;
use App\Order;
use App\OrderItem;
use Notification;
use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use App\Notifications\UserNotification;
use App\Notifications\AdminNotification;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class EmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    private $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order = Order::where(['id' => $this->id])->first();

        $date = date('m/d/Y');
        $splitDate = explode('/', $date);

        $month = $splitDate[0];
        $day = $splitDate[1];
        $year = $splitDate[2];

        $date = $day . "-".date('F', mktime(0, 0, 0, $month, 10))."-".$year; // March

        $last_name = !empty($splitName[1]) ? $splitName[1] : ''; // If last name doesn't exist, make it empty

        $orderItems = OrderItem::where(['order_id' => $order->id])->get();

        $order->sub_total_price = $order->total_price;
        $order->total_price = (int)$order->sub_total_price + (int) $order->delivery;

        $details = [
            'greeting' => 'Hi '.$order->user_name,
            'subject' => 'Sisterz Kitchen | Order Received | Order Id #'.$order->id,
            'body' => '',
            'thanks' => 'Thank you for choosing Sisterz Kitchen !',
            'order_id' => $order->id
        ];

        $details['body'] = $details['body'] . "<b> Order Id : " . $order->id . "</b><br><br>"; 
        foreach($orderItems as $orderItem){
            $details['body'] = $details['body'] . $orderItem->product_name . " x " . $orderItem->product_quantity . "<br>"; 
        }
        $details['body'] = $details['body'] . "...........................................................<br>";  
        $details['body'] = $details['body'] . "Sub Total : Rs " . $order->sub_total_price . "<br>";  
        $details['body'] = $details['body'] . "Delivery : Rs " . $order->delivery . "<br>";  
        $details['body'] = $details['body'] . "Total : Rs " . $order->total_price . "<br>";  
        $details['body'] = $details['body'] . "............................................................<br>";  
        $details['body'] = $details['body'] . "Placed At : " .$date ." <br>";  
        $emailBody = $details['body'];
        $details['body'] = $details['body'] . "For assistance, feel free to call us at this number +92 333 8071764 <br>";  

        $order->email = $order->user_email;
        Notification::send($order , new UserNotification($details));

        $details = [
            'subject' => 'New order recieved',
            'body' => '',
        ];
        $details['body'] = $emailBody;        
        $details['body'] = $details['body'] . "............................................................<br>";  
        $details['body'] = $details['body'] . "Order Time : " . $order->created_at . "<br>";        
        $details['body'] = $details['body'] . "Customer Name : " . $order->user_name . "<br>";        
        $details['body'] = $details['body'] . "Customer Phone : " . $order->user_phone . "<br>";        
        $details['body'] = $details['body'] . "Customer Address : " . $order->user_address . "<br>";        

        $user = new User();
        $user->email="sisterzkitchen1@gmail.com";
        Notification::send($user , new AdminNotification($details));
    }
}
