<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class Client extends Model
{
    use HasApiTokens;

    protected $fillable = [
        'name' , 
        'email', 
        'phone' , 
        'address',
        'token',
        'android_id',
        'verified'
    ];
}
