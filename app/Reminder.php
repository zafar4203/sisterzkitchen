<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reminder extends Model
{
    protected $fillable = [
        'title' , 
        'description', 
        'amount' , 
        'status' , 
        'submit_date'];


        public static function block($val){
            if($val == 0)
            return "Active";
            else
            return "Passed";
        }
}
