<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Products Controller
Route::get('products' , 'Api\ProductController@products');
Route::get('productsWeb' , 'Api\ProductController@productsWeb');
Route::get('productsMob' , 'Api\ProductController@productsMob');
Route::get('productsAdm' , 'Api\ProductController@productsAdm');

// User Controller
Route::post('login', 'Api\UserController@login');
Route::post('register', 'Api\UserController@register');

// Order Controller
Route::post('placeOrder', 'Api\OrderController@placeOrderWeb');
Route::post('placeOrderAndroid', 'Api\OrderController@placeOrderAndroid');
Route::post('orderItems', 'Api\OrderController@orderItems');
Route::post('orderCancel' , 'Api\OrderController@orderCancel');
Route::post('sendOrderEmails' , 'Api\OrderController@sendOrderEmails');
Route::get('orders/{phone}' , 'Api\OrderController@getOrders');

// ClientController
Route::post('addClient', 'Api\ClientController@addClient');
Route::get('getClient/{android_id}', 'Api\ClientController@getClient');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});