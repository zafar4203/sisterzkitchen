<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/download/{file}', function ($file='') {
    return response()->download(public_path('files/'.$file)); 
});
Route::get('zipFiles/{names}' , 'FileController@zipFile');

Route::group(['middleware' => ['auth:web,staff','prevent-back-history']], function() {
Route::group(['middleware' => ['permission:products|all']], function() {
    // Product Controller
    Route::resource('products' , 'ProductController');
    Route::get('ajaxProducts','ProductController@getAjaxProducts')->name('ajaxProducts');
    Route::get('deleteProduct/{id}','ProductController@deleteProduct');
    Route::post('updateProduct' , 'ProductController@update');
});

Route::group(['middleware' => ['permission:orders|all']], function() {
    // Order Controller
    Route::get('ajaxOrders/{year}/{month}','OrderController@getOrders');
    Route::get('orders' , 'OrderController@index');
    Route::get('addOrder' , 'OrderController@create');
    Route::post('addOrder' , 'OrderController@store')->name('addOrder');
    Route::get('editOrder/{id}','OrderController@edit')->name('editOrder');
    Route::post('updateOrder' , 'OrderController@update')->name('updateOrder');
    Route::get('deleteOrder/{id}','OrderController@delete');

    // Order Details and Modify Order
    Route::get('orderUpdate/{id}' , 'OrderController@updateOrder');
    Route::get('orderDetails/{id}' , 'OrderController@orderDetails');
    Route::get('deleteOrderItem/{id}/{order_item}' , 'OrderController@deleteOrderItem');
    Route::get('orderStatus/{id}/{status}' , 'OrderController@changeOrderStatus');
    Route::post('orderUserAddress' , 'OrderController@orderUserAddress');
    Route::post('orderChange' , 'OrderController@orderChange');
    Route::post('updateOrderItems', 'OrderController@updateOrderItems');

});

Route::group(['middleware' => ['permission:sales|all']], function() {
    // Sales Routes
    Route::get('ajaxSales/{year}/{month}','SalesController@getSales');
    Route::get('ajaxSalesItems/{year}/{month}','SalesController@getSalesItems');
    Route::get('ajaxSalesDate/{start}/{end}','SalesController@getSalesDate');
    Route::get('sales' , 'SalesController@index');
    Route::get('saleDetails/{date}' , 'SalesController@saleDetails');
    Route::get('saleItemsMonth' , 'SalesController@saleItemsMonth');
});

Route::group(['middleware' => ['permission:financials|all']], function() {
    //Financials Routes
    Route::get('financials' , 'FinancialController@financials');
    Route::get('ajaxFinancials/{year}/{month}' , 'FinancialController@getFinancials');
    Route::get('ajaxFinancialDates/{start}/{end}' , 'FinancialController@getFinancialDates');

    //Expenses Routes
    Route::get('expenses' , 'ExpenseController@index');
    Route::get('addExpense' , 'ExpenseController@create');
    Route::get('editExpense/{id}' , 'ExpenseController@edit')->name('expenses.edit');
    Route::post('addExpense' , 'ExpenseController@store');
    Route::post('editExpense' , 'ExpenseController@update');
    Route::get('deleteExpense/{id}' , 'ExpenseController@delete');
    Route::get('ajaxExpenses/{year}/{month}','ExpenseController@getExpenses');

    //Revenues Routes
    Route::get('revenues' , 'RevenueController@index');
    Route::get('addRevenue' , 'RevenueController@create');
    Route::get('editRevenue/{id}' , 'RevenueController@edit')->name('revenues.edit');
    Route::post('addRevenue' , 'RevenueController@store');
    Route::post('editRevenue' , 'RevenueController@update');
    Route::get('deleteRevenue/{id}' , 'RevenueController@delete');
    Route::get('ajaxRevenues/{year}/{month}','RevenueController@getRevenues');

});

Route::group(['middleware' => ['permission:files|all']], function() {
    // Files Routes
    Route::get('files' , 'FileController@index');
    Route::get('addFile' , 'FileController@create');
    Route::post('addFile' , 'FileController@store');
    Route::get('deleteFile/{id}' , 'FileController@delete');
});

Route::group(['middleware' => ['permission:testimonials|all']], function() {

    //Testimonial Routes
    Route::get('testimonials' , 'TestimonialController@index');
    Route::get('addTestimonial' , 'TestimonialController@create');
    Route::get('editTestimonial/{id}' , 'TestimonialController@edit')->name('testimonials.edit');
    Route::post('addTestimonial' , 'TestimonialController@store');
    Route::post('editTestimonial' , 'TestimonialController@update');
    Route::get('deleteTestimonial/{id}' , 'TestimonialController@delete');
    Route::get('ajaxTestimonials','TestimonialController@getTestimonials');
});


Route::group(['middleware' => ['permission:staff|all']], function() {
    //Staff Routes
    Route::get('staff' , 'StaffController@index')->name('staff');
    Route::get('addStaff' , 'StaffController@create');
    Route::get('editStaff/{id}' , 'StaffController@edit')->name('staff.edit');
    Route::post('addStaff' , 'StaffController@store');
    Route::post('editStaff' , 'StaffController@update');
    Route::get('deleteStaff/{id}' , 'StaffController@delete');
    Route::get('ajaxStaff','StaffController@getStaff');


    //Roles Routes
    Route::get('roles' , 'RolesController@index');
    Route::get('addRole' , 'RolesController@create');
    Route::get('editRole/{id}' , 'RolesController@edit')->name('roles.edit');
    Route::post('addRole' , 'RolesController@store');
    Route::post('editRole' , 'RolesController@update');
    Route::get('deleteRole/{id}' , 'RolesController@delete');
    Route::get('ajaxRoles','RolesController@getRoles');

    // Permission Route
    Route::get('permissions/{id}', 'RolesController@permission');
    Route::get('addPermission/{role}/{permission}', 'RolesController@addPermission');
    Route::get('removePermission/{role}/{permission}', 'RolesController@removePermission');

    // Client Routes
    Route::get('clients' , 'ClientController@index');
    Route::get('ajaxClients' , 'ClientController@getClients');
});


Route::group(['middleware' => ['permission:reminders|all']], function() {
    //Reminders Routes
    Route::get('reminders' , 'ReminderController@index');
    Route::get('addReminder' , 'ReminderController@create');
    Route::get('editReminder/{id}' , 'ReminderController@edit')->name('reminders.edit');
    Route::post('addReminder' , 'ReminderController@store');
    Route::post('editReminder' , 'ReminderController@update');
    Route::get('deleteReminder/{id}' , 'ReminderController@delete');
    Route::get('ajaxReminders','ReminderController@getReminders');

});

Route::group(['middleware' => ['permission:all|dashboard']], function() {
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index')->name('home');
});

});


Auth::routes(['register' => false]);

Route::get('meme', 'Auth\RegisterController@showRegistrationForm');
Route::post('meme', 'Auth\RegisterController@register')->name('register');

Route::get('checkReminder', 'ReminderController@checkReminder');
Route::get('/logout', 'Auth\LoginController@logout');